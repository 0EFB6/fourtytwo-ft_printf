/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utils.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/18 12:43:52 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/18 12:56:05 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_nbrlen_c(long n, int base)
{
	int	count;

	if (n == 0)
		return (1);
	count = 0;
	if (!base)
		base = 10;
	while (n != 0)
	{
		n /= base;
		count++;
	}
	return (count);
}

int	ft_putnchar_fd(char c, int fd, int n)
{
	int	i;

	i = 0;
	while (n-- > 0)
		i += (int)write(fd, &c, 1);
	return (i);
}

int	ft_putnstr_fd(char *str, int fd, int n)
{
	if (str != NULL)
		return ((int)write(fd, str, n));
	return (0);
}

char	*ft_uitoa(unsigned int n)
{
	int				len;
	char			*str;

	len = ft_nbrlen_c(n, 10);
	str = (char *)malloc(len + 1);
	MALLOC_NULL_CHECK(str);
	str[len] = '\0';
	while (len-- > 0)
	{
		str[len] = n % 10 + '0';
		n /= 10;
	}
	return (str);
}

t_format	ft_format(void)
{
	t_format	format;

	format.minus = 0;
	format.plus = 0;
	format.width = 0;
	format.precision = 0;
	format.negative_precision = 0;
	format.zero = 0;
	format.dot = 0;
	format.space = 0;
	format.hash = 0;
	format.specifier = 0;
	return (format);
}
