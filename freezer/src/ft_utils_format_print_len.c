/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utils_format_print.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/27 15:07:53 by cwei-she          #+#    #+#             */
/*   Updated: 2023/07/28 18:07:51 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_len(const char *str)
{
	const char	*ptr;

	ptr = str;
	while (*ptr)
		ptr++;
	return (ptr - str);
}

static int	ft_is_valid_specifier(char c, t_format *f)
{
	if (ft_strchr(SPECIFIERS, c))
	{
		f->specifier = c;
		return (1);
	}
	return (0);
}

char	*ft_get_format_m(const char *str, t_format *f)
{
	if (str[1] && ft_is_valid_specifier(str[1], f))
		return (ft_set_format(ft_set_format(NULL, '%', 0), str[1], 1));
	return (NULL);
}

int	ft_print_char_m(char *str, int n)
{
	int	i;

	i = 0;
	if (n == 0)
		n = 1;
	while (i < n)
		write(1, &str[i++], 1);
	return (i);
}

int	ft_print_str_m(char *str)
{
	ft_putstr_fd(str, 1);
	return (ft_strlen(str));
}
