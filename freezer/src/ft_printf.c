/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/15 11:34:35 by cwei-she          #+#    #+#             */
/*   Updated: 2023/07/28 18:39:20 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*ft_parse_m(va_list arg, t_format *f)
{
	char	*str;
	char	specifier;

	specifier = f->specifier;
	if (specifier == 'c')
		str = ft_specifier_c(va_arg(arg, int));
	else if (specifier == 's')
		str = ft_specifier_s(va_arg(arg, char *));
	else if (specifier == 'p')
		str = ft_specifier_p((unsigned long long)va_arg(arg, void *));
	else if (specifier == 'd' || specifier == 'i')
		str = ft_specifier_d_i(va_arg(arg, int));
	else if (specifier == 'u')
		str = ft_specifier_u(va_arg(arg, int));
	else if (specifier == 'x')
		str = ft_specifier_x(va_arg(arg, unsigned int), 0);
	else if (specifier == 'X')
		str = ft_specifier_x(va_arg(arg, unsigned int), 1);
	else if (specifier == '%')
		str = ft_specifier_c('%');
	else
		str = ft_calloc(sizeof(char), 1);
	MALLOC_NULL_CHECK(str);
	return (str);
}

/* The ft_print_char() function will print the string */
/* If the format specifier is 'c', it will print the character using the ft_print_char() fucntion */
/* Or else, it will print the string using the ft_print_str_m() function */
static int	ft_print(va_list arg, t_format *f)
{
	int		ret;
	char	*str;

	str = ft_parse_m(arg, f);
	if (f->specifier == 'c')
		ret = ft_print_char_m(str, 1);
	else
		ret = ft_print_str_m(str);
	free(str);
	return (ret);
}

/* The ft_run() function will loop through the string and check for '%' */
/* If it encounters '%', it will call ft_get_format_m() to parse the	 */
/* string and return the format string (eg. "%s" or "%p").               */
/* If it is not NULL, it will call ft_print() to print the string		 */
/* If it is NULL, it will print the string using ft_putchar_fd()		 */
/* The while loop will loop till when *str is NULL and return the return */
/* value which is the number of character printed out					 */
static int	ft_run(const char *str, va_list arg)
{
	int			ret;
	char		*format;
	t_format	f;
	
	ret = 0;
	while (*str)
	{
		if (*str == '%')
		{
			format = ft_get_format_m(str, &f);
			if (format)
			{
				ret += ft_print(arg, &f);
				str += ft_strlen(format);
				free(format);
			}
		}
		else
		{
			ft_putchar_fd(*str, 1);
			ret++;
			str++;
		}
	}
	return (ret);
}

/* The main printf function for my own custom implementation called ft_printf */
/* va_start() initializes the arg_ptr pointer for subsequent calls to		  */
/* va_arg() and va_end().													  */
/* The va_arg() function retrieves a value of the given var_type from the	  */
/* location given by arg_ptr, and increases arg_ptr to point to the next	  */
/* argument in the list. The va_arg() function can retrieve arguments from	  */
/* the list any number of times within the function. The var_type argument	  */
/* must be one of int, long, decimal, double, struct, union, or pointer, or	  */
/* a typedef of one of these types.											  */
/* The va_end() function is needed to indicate the end of parameter scanning. */
/* When encounter '%', execute the parser until it encounters SPECIFIERS	  */
/* and print accordingly to the specifiers									  */
/* If not, write to standard output using ft_putnchar_fd()					  */
/* The while loop will loop till when *str is NULL and va_end the argument	  */
/* Lastly, return the return value which is the number of character printed	  */
/* out																		  */
int	ft_printf(const char *str, ...)
{
	int			ret;
	va_list		arg;

	if (str == NULL)
		return (0);
	va_start(arg, str);
	ret = ft_run(str, arg);
	va_end(arg);
	return (ret);
}
