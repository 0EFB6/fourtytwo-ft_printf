/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arg_c_d_i_s_u_x.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/27 12:04:26 by cwei-she          #+#    #+#             */
/*   Updated: 2023/07/28 17:11:12 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*ft_specifier_c(char c)
{
	char	*ret;

	ret = ft_calloc(sizeof(char), 2);
	MALLOC_NULL_CHECK(ret);
	ret[0] = c;
	return (ret);
}

char	*ft_specifier_d_i(int nbr)
{
	char	*ret;

	ret = ft_itoa(nbr);
	MALLOC_NULL_CHECK(ret);
	return (ret);
}

char	*ft_specifier_s(char *str)
{
	char	*ret;

	if (str)
		ret = ft_calloc(sizeof(char), ft_strlen(str) + 1);
	else
		ret = ft_calloc(sizeof(char), S_EMPTY_LEN + 1);
	MALLOC_NULL_CHECK(ret);
	if (str)
		ret = ft_memcpy(ret, str, ft_strlen(str));
	else
		ret = ft_memcpy(ret, S_EMPTY, S_EMPTY_LEN);
	return (ret);
}

char	*ft_specifier_u(unsigned int nbr)
{
	char	*ret;

	ret = ft_uitoa(nbr);
	MALLOC_NULL_CHECK(ret);
	return (ret);
}

char	*ft_specifier_x(unsigned int nbr, int is_upper)
{
	char	*ret;

	ret = ft_calloc(sizeof(char), ft_baselen(nbr) + 1);
	MALLOC_NULL_CHECK(ret);
	ret = ft_itoh(nbr, is_upper, ret);
	return (ret);
}
