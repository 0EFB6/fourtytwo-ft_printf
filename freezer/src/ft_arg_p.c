/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arg_p.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/27 12:17:55 by cwei-she          #+#    #+#             */
/*   Updated: 2023/07/28 17:27:39 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*ft_specifier_p(size_t str)
{
	char	*ret;
	char	*hex;
	char	*tmp;

	if (!str)
	{
		ret = ft_calloc(sizeof(char), P_EMPTY_LEN + 1);
		MALLOC_NULL_CHECK(ret);
		ret = ft_memcpy(ret, P_EMPTY, P_EMPTY_LEN);
		return (ret);
	}
	tmp = ft_calloc(sizeof(char), 3);
	MALLOC_NULL_CHECK(tmp);
	tmp[0] = '0';
	tmp[1] = 'x';
	hex = ft_calloc(sizeof(char), ft_baselen(str) + 1);
	MALLOC_NULL_CHECK(hex);
	hex = ft_itoh(str, 0, hex);
	ret = ft_strjoin(tmp, hex);
	free(hex);
	free(tmp);
	return (ret);
}
