/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utils_reset_format_parse.c                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/27 15:07:53 by cwei-she          #+#    #+#             */
/*   Updated: 2023/07/28 18:38:39 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_reset(t_format *f)
{
	f->minus = 0;
	f->plus = 0;
	f->space = 0;
	f->zero = 0;
	f->hash = 0;
	f->dot = 0;
	f->width = 0;
	f->precision = 0;
	f->specifier = 0;
}

static int	ft_set_star(char c, va_list arg, t_format *f)
{
	int	star;

	if (c != '*')
		return (0);
	if (f->dot)
	{
		star = (va_arg(arg, int));
		f->precision = star;
	}
	else
	{
		star = (va_arg(arg, int));
		if (star < 0)
		{
			f->minus = 1;
			star *= -1;
		}
		f->width = star;
	}
	return (1);
}

static int	ft_is_valid_specifier(char c, va_list arg, t_format *f)
{
	if (ft_set_star(c, arg, f))
		return (1);
	else if (f->dot && ft_isdigit(c))
		f->precision = f->precision * 10 + (c - '0');
	else if (ft_strchr(SPECIFIERS, c))
		f->specifier = c;
	else if (c == '-')
		f->minus = 1;
	else if (c == '0' && f->width == 0)
		f->zero = 1;
	else if (c == '.')
		f->dot = 1;
	else if (c == '#')
		f->hash = 1;
	else if (c == ' ')
		f->space = 1;
	else if (c == '+')
		f->plus = 1;
	else if (ft_isdigit(c))
		f->width = f->width * 10 + (c - '0');
	else
		return (0);
	return (1);
}

char	*ft_get_format_b(const char *str, va_list arg, t_format *f)
{
	int		i;
	char	*format;

	format = ft_set_format(NULL, '%', 0);
	MALLOC_NULL_CHECK(format);
	i = 1;
	ft_reset(f);
	while (str[i])
	{
		if (!ft_is_valid_specifier(str[i], arg, f))
		{	
			free(format);
			return (NULL);
		}
		format = ft_set_format(format, str[i], i);
		if (!str)
			return (NULL);
		if (f->specifier)
			return (format);
		i++;
	}
	free(format);
	return (NULL);
}

char	*ft_parse_b(va_list arg, t_format *f)
{
	char	*str;
	char	specifier;

	specifier = f->specifier;
	if (specifier == 'c')
		str = ft_arg_c(va_arg(arg, int));
	else if (specifier == 's')
		str = ft_arg_s(va_arg(arg, char *));
	else if (specifier == 'p')
		str = ft_arg_p((unsigned long long)va_arg(arg, void *));
	else if (specifier == 'd' || specifier == 'i')
		str = ft_arg_d_i(va_arg(arg, int));
	else if (specifier == 'u')
		str = ft_arg_u(va_arg(arg, int));
	else if (specifier == 'x')
		str = ft_arg_x(va_arg(arg, unsigned int), 0);
	else if (specifier == 'X')
		str = ft_arg_x(va_arg(arg, unsigned int), 1);
	else if (specifier == '%')
		str = ft_arg_c('%');
	else
		str = ft_calloc(sizeof(char), 1);
	MALLOC_NULL_CHECK(str);
	return (str);
}
