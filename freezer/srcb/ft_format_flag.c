/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_format_flag.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/27 16:24:34 by cwei-she          #+#    #+#             */
/*   Updated: 2023/07/28 18:11:51 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static char	*ft_format_plus(char *str, t_format *f)
{
	int		i;
	char	*ret;
	char 	*tmp;

	if ((f->plus || f->space) && (f->specifier == 'd' || f->specifier == 'i'))
	{
		i = ft_atoi(str);
		if (i < 0)
			return (str);
		ret = ft_calloc(sizeof(char), 2);
		MALLOC_NULL_CHECK(ret);
		if (f->plus)
			ret[0] = '+';
		else
			ret[0] = ' ';
		tmp = ft_strjoin(ret, str);
		MALLOC_NULL_CHECK(tmp);
		free(ret);
		ret = tmp;
		free(str);
		return (ret);
	}
	return (str);
}

static char	*ft_format_hash(char *str, t_format *f)
{
	char	*ret;
	char	*tmp;

	if (f->hash && (f->specifier == 'x' || f->specifier == 'X'))
	{
		ret = ft_calloc(sizeof(char), 3);
		MALLOC_NULL_CHECK(ret);
		ret[0] = '0';
		if (str[0] != '0')
		{
			ret[1] = f->specifier;
			tmp = ft_strjoin(ret, str);
			MALLOC_NULL_CHECK(tmp);
			free(ret);
			ret = tmp;
		}
		free(str);
		return (ret);
	}
	return (str);
}

char	*ft_flag(char *str, t_format *f)
{
	str = ft_format_plus(str, f);
	MALLOC_NULL_CHECK(str);
	str = ft_format_hash(str, f);
	MALLOC_NULL_CHECK(str);
	return (str);
}
