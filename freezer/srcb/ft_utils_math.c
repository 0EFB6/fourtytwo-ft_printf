/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utils_math.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/28 18:04:50 by cwei-she          #+#    #+#             */
/*   Updated: 2023/07/28 18:04:55 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	ft_nbrlen(unsigned int nbr)
{
	if (nbr < 10)
		return (1);
	return (1 + ft_nbrlen(nbr / 10));
}

int	ft_baselen(size_t nbr)
{
	if (nbr < 16)
		return (1);
	return (1 + ft_baselen(nbr / 16));
}

char	*ft_uitoa(unsigned int n)
{
	int		len;
	char	*str;

	len = ft_nbrlen(n);
	str = (char *)malloc(len + 1);
	MALLOC_NULL_CHECK(str);
	str[len] = '\0';
	while (len-- > 0)
	{
		str[len] = n % 10 + '0';
		n /= 10;
	}
	return (str);
}

char	*ft_itoh(size_t nbr, int is_upper, char *buffer)
{
	int			i;
	const char	*hex;

	i = 0;
	buffer[i] = '0';
	if (is_upper)
		hex = HEX_UP;
	else
		hex = HEX_LOWER;
	while (nbr > 0)
	{
		buffer[i] = hex[nbr % 16];
		nbr /= 16;
		i++;
	}
	ft_strrev(buffer);
	return (buffer);
}
