/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_format_precision.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/27 16:49:41 by cwei-she          #+#    #+#             */
/*   Updated: 2023/07/28 14:53:38 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static char	*ft_precision_string(char *str, t_format *f)
{
	char	*ret;

	if ((f->dot && f->precision == 0)
		|| (IS_LINUX
			&& (ft_strncmp(str, S_EMPTY, S_EMPTY_LEN) == 0
				&& f->precision < 6)))
		ret = ft_calloc(sizeof(char), 1);
	else
		ret = ft_substr(str, 0, f->precision);
	MALLOC_NULL_CHECK(ret);
	free(str);
	return (ret);
}

static char	*ft_precision_number_fill(char *str, char *ret, t_format *f)
{
	int	i;
	int	j;
	int	len;

	i = 0;
	j = 0;
	len = ft_strlen(str);
	if (str[0] == '-')
	{
		i++;
		j++;
		len--;
		f->precision++;
	}
	while (i < f->precision)
	{
		if (i < f->precision - len)
			ret[i] = '0';
		else
			ret[i] = str[j++];
		i++;
	}
	return (ret);
}

static char	*ft_precision_number_helper(char *str, t_format *f)
{
	char	*ret;

	if (str[0] == '-')
	{
		ret = ft_calloc(sizeof(char), f->precision + 2);
		MALLOC_NULL_CHECK(ret);
		ret[0] = '-';
	}
	else
	{
		ret = ft_calloc(sizeof(char), f->precision + 1);
		MALLOC_NULL_CHECK(ret);
	}
	ft_precision_number_fill(str, ret, f);
	return (ret);
}

static char	*ft_precision_number(char *str, t_format *f)
{
	char	*ret;

	if (str[0] == '0' && f->precision == 0)
	{
		ret = ft_calloc(sizeof(char), f->precision + 1);
		MALLOC_NULL_CHECK(ret);
		free (str);
		return (ret);
	}
	else if (f->precision >= (int)ft_strlen(str))
	{
		ret = ft_precision_number_helper(str, f);
		MALLOC_NULL_CHECK(ret);
		free(str);
		return (ret);
	}
	return (str);
}

char	*ft_precision(char *str, t_format *f)
{
	char	*ret;

	if (!f->dot)
		return (str);
	if (f->specifier == 's' || f->specifier == 'c')
		ret = ft_precision_string(str, f);
	else
		ret = ft_precision_number(str, f);
	MALLOC_NULL_CHECK(ret);
	return (ret);
}
