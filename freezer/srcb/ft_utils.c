/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utils.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/28 18:03:02 by cwei-she          #+#    #+#             */
/*   Updated: 2023/07/28 18:39:25 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*ft_set_format(char *str, char c, int i)
{
	if (!str)
		str = ft_calloc(sizeof(char), i + 2);
	else
		str = ft_realloc(str, i + 2);
	MALLOC_NULL_CHECK(str);
	str[i] = c;
	return (str);
}

char	*ft_strrev(char *str)
{
	int		i;
	int		j;
	char	tmp;

	i = 0;
	j = ft_strlen(str) - 1;
	while (i < j)
	{
		tmp = str[j];
		str[j] = str[i];
		str[i] = tmp;
		i++;
		j--;
	}
	return (str);
}

void	*ft_realloc(void *ptr, size_t size)
{
	void	*ret;
	char	*tmp;
	size_t	ptrsize;

	ptrsize = 0;
	tmp = (char *)ptr;
	while (*tmp++)
		ptrsize++;
	if (size <= ptrsize)
		return (ptr);
	ret = malloc(size * sizeof(char));
	MALLOC_NULL_CHECK(ret);
	ft_bzero(ret, size);
	if (!ptr)
		return (ret);
	ft_memcpy(ret, ptr, ptrsize);
	free(ptr);
	return (ret);
}
