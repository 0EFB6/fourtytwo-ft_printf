/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utils_print.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/27 15:08:35 by cwei-she          #+#    #+#             */
/*   Updated: 2023/07/28 18:09:53 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_len(const char *str)
{
	int	i;

	i = 0;
	while (str[i] != '\0')
		i++;	
	if (i == 0)
		return (1);
	return (i);
}

/* Write out characters to standard output recursively based on the n number */
/* given																	 */
int	ft_putnchar_fd(char c, int fd, int n)
{
	int	i;

	i = 0;
	while (n-- > 0)
		i += (int)write(fd, &c, 1);
	return (i);
}

int	ft_print_char(char *str, int n)
{
	int	i;

	i = 0;
	if (n == 0)
		n = 1;
	while (i < n)
	{
		write(1, &str[i], 1);
		i++;
	}
	return (i);
}

int	ft_print_str(char *str, t_format *f)
{
	int	i;

	if ((f->specifier == 's' && f->dot && f->precision == 0) || (str[0] == 0))
		return (ft_print_empty(str, f));
	i = ft_strlen(str);
	if (f->width > 0 && i == 0)
		i = ft_print_char(str, f->width);
	else
		ft_putstr_fd(str, 1);
	return (i);
}

int	ft_print_empty(char *str, t_format *f)
{
	int	i;

	i = 0;
	if (str[0] == 0 && f->width)
		i = ft_print_char(" ", 1);
	else if (str[0] == 0 && f->dot && f->precision < 0)
		i = ft_print_char(S_EMPTY, S_EMPTY_LEN);
	else if (ft_strlen(str) > 0 && f->width > 0)
	{
		while (i++ < f->width)
			ft_putchar_fd(' ', 1);
		i--;
	}
	return (i);
}
