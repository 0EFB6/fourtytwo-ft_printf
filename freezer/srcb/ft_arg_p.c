/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arg_p.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/27 12:17:55 by cwei-she          #+#    #+#             */
/*   Updated: 2023/07/28 14:53:30 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*ft_arg_p(size_t str)
{
	char	*hex;
	char	*ret;
	char	*tmp;
	char	*buffer;

	if (!str)
	{
		ret = ft_calloc(sizeof(char), P_EMPTY_LEN + 1);
		MALLOC_NULL_CHECK(ret);
		ret = ft_memcpy(ret, P_EMPTY, P_EMPTY_LEN);
		return (ret);
	}
	ret = ft_calloc(sizeof(char), 3);
	MALLOC_NULL_CHECK(ret);
	ret[0] = '0';
	ret[1] = 'x';
	buffer = ft_calloc(sizeof(char), ft_baselen(str) + 1);
	MALLOC_NULL_CHECK(buffer);
	hex = ft_itoh(str, 0, buffer);
	if (!hex)
	{
		free(buffer);
		return (NULL);
	}
	tmp = ft_strjoin(ret, hex);
	MALLOC_NULL_CHECK(tmp);
	free(ret);
	ret = tmp;
	if (hex)
		free(hex);
	return (ret);
}
