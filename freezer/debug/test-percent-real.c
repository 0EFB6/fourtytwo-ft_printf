/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test-percent-real.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/16 00:48:57 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/16 11:56:19 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int	main(void)
{
	int		tmp1;
	int		tmp2;
	int		tmp3;
	char	str[] = "hello world!";

	tmp1 = printf("%%\n");
	printf("Return value for printf: %d\n", tmp1);
	tmp2 = printf("Testing multiple %% signs: %% %% %%\n");
	printf("Return value for printf: %d\n", tmp2);
	tmp3 = printf("String with %%: %s\n", str);
	printf("Return value for printf: %d\n", tmp3);
	return (0);
}
