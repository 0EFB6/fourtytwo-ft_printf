/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test-i.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/15 23:13:29 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/16 11:55:08 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_printf.h"

int	main(void)
{
	int	num;
	int	tmp;

	num = 0;
	tmp = ft_printf("\n");
	printf("Ret value for ft_printf: %d\n", tmp);
	tmp = ft_printf("Case 1: %%i with positive integer: %i\n", 42);
	printf("Ret value for ft_printf: %d\n", tmp);
	tmp = ft_printf("Case 2: %%i with negative integer: %i\n", -42);
	printf("Ret value for ft_printf: %d\n", tmp);
	tmp = ft_printf("Case 3: %%i with zero: %i\n", 0);
	printf("Ret value for ft_printf: %d\n", tmp);
	tmp = ft_printf("Case 4: %%i with INT_MAX: %i\n", 2147483647);
	printf("Ret value for ft_printf: %d\n", tmp);
	tmp = ft_printf("Case 5: %%i with INT_MIN: %i\n", INT_MIN);
	printf("Ret value for ft_printf: %d\n", tmp);
	tmp = ft_printf("Case 6: %%i with octal number: %i\n", 0123);
	printf("Ret value for ft_printf: %d\n", tmp);
	tmp = ft_printf("Case 7: %%i with hexadecimal number: %i\n", 0xFF);
	printf("Ret value for ft_printf: %d\n", tmp);
	tmp = ft_printf("Case 8: %%i with character: %i\n", 'A');
	printf("Ret value for ft_printf: %d\n", tmp);
	tmp = ft_printf("Case 9: %%i with string: %i\n", "123");
	printf("Ret value for ft_printf: %d\n", tmp);
	tmp = ft_printf("Case 10: %%i with pointer: %i\n", &num);
	printf("Ret value for ft_printf: %d\n", tmp);
	return (0);
}
