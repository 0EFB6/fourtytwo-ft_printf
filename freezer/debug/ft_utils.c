/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utils.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/28 18:03:02 by cwei-she          #+#    #+#             */
/*   Updated: 2023/07/29 00:48:43 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

// Set the format string based on the specifier
// If the string is not valid, allocate memory for the string using ft_calloc()
// with size of i + 2 where i will typically be 0
// Else, reallocate memory for the string using ft_realloc() with size of i + 2
// where i will typically be new length of the string
// Check if the allocation is successful
// Set the last character of the string to specifier (c, s, p, d, i, u, x, X,)
// Return the string
char	*ft_set_format(char *str, char c, int i)
{
	if (!str)
		str = ft_calloc(sizeof(char), i + 2);
	else
		str = ft_realloc(str, i + 2);
	MALLOC_NULL_CHECK(str);
	str[i] = c;
	return (str);
}

// Reverse the string
// Set i to 0 and j to the length of the string - 1
// While i is less than j, swap the characters at i and j
// Return the string
char	*ft_strrev(char *str)
{
	int		i;
	int		j;
	char	tmp;

	i = 0;
	j = ft_strlen(str) - 1;
	while (i < j)
	{
		tmp = str[j];
		str[j] = str[i];
		str[i] = tmp;
		i++;
		j--;
	}
	return (str);
}

// Reallocate memory for the string
// If the size is less than or equal to the size of the pointer, return the
// pointer
// Else, allocate memory for return string using ft_calloc() with size of size
// Check if the allocation is successful
// Write 0 to the return string using ft_bzero()
// If the pointer is not valid, return the return string
// Or else, copy the string from the pointer to the return string baased on
// the size of the pointer
// Finally, free the pointer and return the return string
void	*ft_realloc(void *ptr, size_t size)
{
	void	*ret;
	char	*tmp;
	size_t	ptrsize;

	ptrsize = 0;
	tmp = (char *)ptr;
	while (*tmp++)
		ptrsize++;
	if (size <= ptrsize)
		return (ptr);
	ret = malloc(size * sizeof(char));
	MALLOC_NULL_CHECK(ret);
	ft_bzero(ret, size);
	if (!ptr)
		return (ret);
	ft_memcpy(ret, ptr, ptrsize);
	free(ptr);
	return (ret);
}
