/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test-d-real.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/15 23:07:15 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/16 11:54:14 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int	main(void)
{
	int	num;
	int	tmp;

	num = 42;
	tmp = printf("%d\n", num);
	printf("Return value for printf: %d\n", tmp);
	tmp = printf("%d\n", -num);
	printf("Return value for printf: %d\n", tmp);
	tmp = printf("%d\n", 0);
	printf("Return value for printf: %d\n", tmp);
	tmp = printf("%d\n", 1234567890);
	printf("Return value for printf: %d\n", tmp);
	tmp = printf("%d\n", -1234567890);
	printf("Return value for printf: %d\n", tmp);
	tmp = printf("%.2d\n", num);
	printf("Return value for printf: %d\n", tmp);
	return (0);
}
