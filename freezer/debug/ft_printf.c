/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/15 11:34:35 by cwei-she          #+#    #+#             */
/*   Updated: 2023/07/29 00:02:24 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

// A string is initialized to store the string which is to be printed out.
// The string will contains the formatted string with the relevant flags
// such as width, precision and padding. After formatting the string,
// the string is then passed to be printed out.
// Example of formatted string is "   s", "000123" etc.
// If the format specifier is 'c', the string is then passed to ft_print_char()
// to print out the character. Else, the string is then passed to ft_print_str()
// Both printing functions will return the number of character printed out.
// Free the string to avoid memory leak. Lastly, return the return value.
static int	ft_print(va_list arg, t_format *f)
{
	int		ret;
	char	*str;

	str = ft_parse(arg, f);
	str = ft_flag(str, f);
	str = ft_precision(str, f);
	str = ft_width(str, f);
	if (f->specifier == 'c')
		ret = ft_print_char(str, f->width);
	else
		ret = ft_print_str(str, f);
	free(str);
	return (ret);
}

// When encounter '%', execute the ft_get_format() to get the format specifiers
// such as %s or %10d. If format specifier is valid and exist, the relevant
// argument is printed out using ft_print() function. Then, the str pointer will
// be incremented by the length of the format specifier. The format specifier is
// freed to avoid memory leak. Else, the character is printed out using
// ft_putchar_fd() and the str pointer is incremented by 1. The return value is
// also incremented by 1. The main while loop will loop through str until null
// terminator is encountered. Lastly, return the return value which is the
// number of character printed	out
static int	ft_run(const char *str, va_list arg)
{
	int			ret;
	char		*format;
	t_format	f;

	ret = 0;
	while (*str)
	{
		if (*str == '%')
		{
			format = ft_get_format(str, arg, &f);
			if (format)
			{
				ret += ft_print(arg, &f);
				str += ft_strlen(format);
				free(format);
			}
		}
		else
		{
			ft_putchar_fd(*str, 1);
			ret++;
			str++;
		}
	}
	return (ret);
}

// The main printf function for my own custom implementation called ft_printf
// va_start() initializes the arg_ptr pointer for subsequent calls to		 
// va_arg() and va_end().													 
// The va_arg() function retrieves a value of the given var_type from the	 
// location given by arg_ptr, and increases arg_ptr to point to the next	 
// argument in the list. The va_arg() function can retrieve arguments from	 
// the list any number of times within the function. The var_type argument	 
// must be one of int, long, decimal, double, struct, union, or pointer, or	 
// a typedef of one of these types.											 
// The va_end() function is needed to indicate the end of parameter scanning.
//
// After va_start(), ft_run() is called to execute the parser and print
// the output accordingly to the specifiers by passing the argument str and arg
// to ft_run(). The number of character printed out is returned to ret.
// va_end() is called to end the argument
// Lastly, return the return value which is the number of character printed out
int	ft_printf(const char *str, ...)
{
	int			ret;
	va_list		arg;

	if (str == NULL)
		return (0);
	va_start(arg, str);
	ret = ft_run(str, arg);
	va_end(arg);
	return (ret);
}
