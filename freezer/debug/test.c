/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/11 13:27:17 by cwei-she          #+#    #+#             */
/*   Updated: 2023/07/29 14:25:42 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_printf.h"

int	main(void)
{
	int	tmp;
	//int	num = 10;

	//ft_printf("Number: %+i\n", 42);
	//ft_printf("Float: %u\n", 429496739);
	//ft_printf("%+d\n", 2147483647);
	//write(1, "\n\n", 2);
	//ft_printf("h3e %x\n", num);
	//ft_printf("%#x\n", num);
	//ft_printf("%X\n", num);
	//ft_printf("%#X\n", num);
	//char	word[] = "hello world! nice to meet you!";
	//ft_printf("Test%.9s\n", word);
	//ft_printf("Test% d\n", num);
	//int tmp = printf("Test%010u\n", num);
	//int tmp = ft_printf("HAHA%dLOL\n", num);
	//printf("TMP%d\n", tmp);

	//char string[] = "Hello, World!";
	//ft_printf("String with precision: %-s\n", 26, string);
	//ft_printf(" %.2d\n", -1);
	//printf(" %.2d\n", -1);

	//int		str = 123;
	char	str[] = "hello world";

	printf("SPACE============\n");

	tmp = ft_printf("%25p", str);
	printf("\nLEN:%d\n", tmp);

	printf("\nSPACE============\n");

	tmp = printf("%25p", str);
	printf("\nLEN:%d\n", tmp);
	return (0);
}
