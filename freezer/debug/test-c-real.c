/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test-c-real.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/15 23:44:19 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/16 11:53:42 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int	main(void)
{
	char	c;
	int		i;

	c = 'A';
	for (i = 0; i < 10; i++)
	{
		printf("Character: %c\n", c + i);
	}
	return (0);
}
