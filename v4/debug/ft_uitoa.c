/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_uitoa.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/15 21:47:33 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/16 11:42:33 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static char	*ft_init(char *str, int len, unsigned int n)
{
	if (n == 0)
		len = 1;
	str = (char *)malloc(len + 1);
	MALLOC_NULL_CHECK(str);
	str[len] = '\0';
	while (len-- > 0)
	{
		str[len] = n % 10 + '0';
		n /= 10;
	}
	return (str);
}

static int	ft_nbrlen(long n, int base)
{
	int	count;

	if (n == 0)
		return (1);
	count = 0;
	if (!base)
		base = 10;
	while (n != 0)
	{
		n /= base;
		count++;
	}
	return (count);
}

char	*ft_uitoa(unsigned int n)
{
	char			*str;
	int				len;

	str = NULL;
	len = ft_nbrlen(n, 10);
	str = ft_init(str, len, n);
	MALLOC_NULL_CHECK(str);
	return (str);
}
