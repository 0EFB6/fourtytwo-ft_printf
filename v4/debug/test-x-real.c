/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test-x-real.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/16 00:44:22 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/16 13:50:22 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int	main(void)
{
	int	tmp;

	tmp = printf("Unsigned hexadecimal: %x\n", 42);
	printf("Return value for printf: %d\n", tmp);
	tmp = printf("Unsigned hexadecimal: %x\n", -42);
	printf("Return value for printf: %d\n", tmp);
	tmp = printf("Unsigned hexadecimal: %x\n", 0);
	printf("Return value for printf: %d\n", tmp);
	tmp = printf("Unsigned hexadecimal: %x\n", 255);
	printf("Return value for printf: %d\n", tmp);
	tmp = printf("Unsigned hexadecimal: %x\n", 65535);
	printf("Return value for printf: %d\n", tmp);
	tmp = printf("Unsigned hexadecimal: %x\n", 123456789);
	printf("Return value for printf: %d\n", tmp);
	tmp = printf("Unsigned hexadecimal: %x\n", -123456789);
	printf("Return value for printf: %d\n", tmp);
	return (0);
}
