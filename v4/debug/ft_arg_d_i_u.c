/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arg_d_i_u.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/15 16:30:08 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/17 22:43:56 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <limits.h>
#include <stdio.h>
#include "ft_printf.h"

static char	plus(t_format f, char *nbr)
{
	//printf("NBR%d\n", f.plus);
	if (ft_atoi(nbr) == INT_MIN)
		return ('\0');
	if (f.plus)
		return ('+');
	return ('-');
}

static int	ft_nbr(t_format f, char *nbr, int len, int negative)
{
	int	ret;

	ret = 0;
	f.width -= (f.space && !negative && !f.plus && f.width);
	if (negative || f.plus)
		ret += ft_putnchar_fd(plus(f, nbr), 1, f.zero
				&& (!f.dot || f.negative_precision));
	else if (f.space)
		ret += ft_putnchar_fd(' ', 1, f.zero && !f.dot);
	if (!f.minus && f.width > f.precision && (!f.dot
			|| f.negative_precision) && f.zero)
		ret += ft_putnchar_fd('0', 1, f.width - f.precision
				- negative - f.plus);
	else if (!f.minus && f.width > f.precision)
		ret += ft_putnchar_fd(' ', 1, f.width - f.precision
				- negative - f.plus);
	if (negative || f.plus)
		ret += ft_putnchar_fd(plus(f, nbr), 1, !f.zero
				|| (f.dot && !f.negative_precision));
	else if (f.space)
		ret += ft_putnchar_fd(' ', 1, !f.zero || f.dot);
	//printf("RET%d\n", f.precision);
	ret += ft_putnchar_fd('0', 1, f.precision - len);
	//printf("RET%d\n", ret);
	ret += write(1, nbr, len);
	//printf("RET%d\n", ret);
	if (f.minus && f.width > f.precision)
		ret += ft_putnchar_fd(' ', 1, f.width - f.precision
				- negative - f.plus);
	return (ret);
}

int	ft_arg_d_i_u(t_format f, va_list arg)
{
	char	*nbr;
	int		n;
	int		ret;
	int		len;
	int		negative;

	n = va_arg(arg, int);
	ret = 0;
	negative = (n < 0 && n >= INT_MIN && f.specifier != 'u');
	if (negative)
		f.plus = 0;
	if (n < 0 && f.specifier != 'u')
		n *= -1;
	if (n < 0 && f.specifier == 'u')
		nbr = ft_uitoa((unsigned)n);
	else
		nbr = ft_itoa(n);
	len = ft_strlen(nbr);
	if (*nbr == '0' && !f.precision && f.dot)
		len = 0;
	if (f.precision < len || !f.dot)
		f.precision = len;
	//if (*nbr == ' ' && n < 0)
	//	negative = 1;
	ret += ft_nbr(f, nbr, len, negative);
	free(nbr);
	return (ret);
}
