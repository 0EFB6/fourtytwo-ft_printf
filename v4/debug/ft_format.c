/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_format.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/15 12:06:00 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/15 12:16:28 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

t_format	ft_format(void)
{
	t_format	format;

	format.minus = 0;
	format.plus = 0;
	format.width = 0;
	format.precision = 0;
	format.negative_precision = 0;
	format.zero = 0;
	format.dot = 0;
	format.space = 0;
	format.hash = 0;
	format.specifier = 0;
	return (format);
}
