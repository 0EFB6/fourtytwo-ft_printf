/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test-s.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/16 00:36:36 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/16 11:58:53 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_printf.h"

int	main(void)
{
	char	str1[] = "Hello";
	char	str2[] = "World!";
	char	str3[] = "";
	char	str4[] = "This is a long string that exceeds the buffer size.";
	char	*str5 = NULL;
	char	str6[] = "1234567890";
	char	str7[] = "abcdefghijklmnopqrstuvwxyz";
	char	str8[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	char	str9[] = "0123456789";
	char	str10[] = "!@#$%^&*()";
	int		ret1 = ft_printf("String: %s\n", str1);
	int		ret2 = ft_printf("String: %s\n", str2);
	int		ret3 = ft_printf("String: %s\n", str3);
	int		ret4 = ft_printf("String: %s\n", str4);
	int		ret5 = ft_printf("String: %s\n", str5);
	int		ret6 = ft_printf("String: %s\n", str6);
	int		ret7 = ft_printf("String: %s\n", str7);
	int		ret8 = ft_printf("String: %s\n", str8);
	int		ret9 = ft_printf("String: %s\n", str9);
	int		ret10 = ft_printf("String: %s\n", str10);

	printf("Return values for ft_printf:\n");
	printf("1: %d\n", ret1);
	printf("2: %d\n", ret2);
	printf("3: %d\n", ret3);
	printf("4: %d\n", ret4);
	printf("5: %d\n", ret5);
	printf("6: %d\n", ret6);
	printf("7: %d\n", ret7);
	printf("8: %d\n", ret8);
	printf("9: %d\n", ret9);
	printf("10: %d\n", ret10);
	return (0);
}
