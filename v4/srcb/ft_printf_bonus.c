/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_bonus.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/15 11:34:35 by cwei-she          #+#    #+#             */
/*   Updated: 2023/07/27 17:49:38 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_bonus.h"

static int	ft_print(va_list arg, t_format *f)
{
	int		ret;
	char	*str;

	str = ft_parse(arg, f);
	str = ft_flag(str, f);
	str = ft_precision(str, f);
	str = ft_width(str, f);
	if (f->specifier == 'c')
		ret = ft_print_char(str, f->width);
	else
		ret = ft_print_str(str, f);
	free(str);
	return (ret);
}

static int	ft_run(const char *str, va_list arg)
{
	int			ret;
	char		*format;
	t_format	f;
	
	ret = 0;
	while (*str)
	{
		if (*str == '%')
		{
			format = ft_get_format(str, arg, &f);
			if (format)
			{
				ret += ft_print(arg, &f);
				str += ft_strlen(format);
				free(format);
			}
			//c = (char *)str;
			//if (*(++str))
			//	ret += ft_parse((char *)str, arg);
			//while (*str && !ft_strchr(SPECIFIERS, *str))
			//	str++;
			//if (!(*str))
			//	str = c;
		}
		else
		{
			ft_putchar_fd(*str, 1);
			ret++;
			str++;
		}
	}
	return (ret);
}

/* The main printf function for my own custom implementation called ft_printf */
/* va_start() initializes the arg_ptr pointer for subsequent calls to		  */
/* va_arg() and va_end().													  */
/* The va_arg() function retrieves a value of the given var_type from the	  */
/* location given by arg_ptr, and increases arg_ptr to point to the next	  */
/* argument in the list. The va_arg() function can retrieve arguments from	  */
/* the list any number of times within the function. The var_type argument	  */
/* must be one of int, long, decimal, double, struct, union, or pointer, or	  */
/* a typedef of one of these types.											  */
/* The va_end() function is needed to indicate the end of parameter scanning. */
/* When encounter '%', execute the parser until it encounters SPECIFIERS	  */
/* and print accordingly to the specifiers									  */
/* If not, write to standard output using ft_putnchar_fd()					  */
/* The while loop will loop till when *str is NULL and va_end the argument	  */
/* Lastly, return the return value which is the number of character printed	  */
/* out																		  */
int	ft_printf(const char *str, ...)
{
	int			ret;
	va_list		arg;

	if (str == NULL)
		return (0);
	va_start(arg, str);
	ret = ft_run(str, arg);
	va_end(arg);
	return (ret);
}
