/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arg_p.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/27 12:17:55 by cwei-she          #+#    #+#             */
/*   Updated: 2023/07/27 15:22:57 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_bonus.h"

char	*ft_arg_p(size_t str)
{
	char	*hex;
	char	*ret;

	if (!str)
	{
		ret = ft_calloc(P_EMPTY_LEN + 1, sizeof(char));
		MALLOC_NULL_CHECK(ret);
		ret = ft_memcpy(ret, P_EMPTY, P_EMPTY_LEN);
		return (ret);
	}
	ret = ft_calloc(3, sizeof(char));
	MALLOC_NULL_CHECK(ret);
	ret[0] = '0';
	ret[1] = 'x';
	hex = ft_itoh(str, 0);
	MALLOC_NULL_CHECK(hex);
	ret = ft_strjoin(ret, hex);
	MALLOC_NULL_CHECK(ret);
	if (hex)
		free(hex);
	return (ret);
}
