/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utils_format.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/27 15:07:53 by cwei-she          #+#    #+#             */
/*   Updated: 2023/07/27 23:22:51 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_bonus.h"

static int	ft_set_star(char c, va_list arg, t_format *f)
{
	int	star;

	if (c != '*')
		return (0);
	if (f->dot)
	{
		star = (va_arg(arg, int));
		f->precision = star;
	}
	else
	{
		star = (va_arg(arg, int));
		if (star < 0)
		{
			f->minus = 1;
			star *= -1;
		}
		f->width = star;
	}
	return (1);
}

static int	ft_set_format(char c, va_list arg, t_format *f)
{
	if (ft_set_star(c, arg, f))
		return (1);
	else if (f->dot && ft_isdigit(c))
		f->precision = f->precision * 10 + (c - '0');
	else if (ft_strchr(SPECIFIERS, c))
		f->specifier = c;
	else if (c == '-')
		f->minus = 1;
	else if (c == '0' && f->width == 0)
		f->zero = 1;
	else if (c == '.')
		f->dot = 1;
	else if (c == '#')
		f->hash = 1;
	else if (c == ' ')
		f->space = 1;
	else if (c == '+')
		f->plus = 1;
	else if (ft_isdigit(c))
		f->width = f->width * 10 + (c - '0');
	else
		return (0);
	return (1);
}

char	*ft_set_format_str(char *str, char c, int i)
{
	if (!str)
		str = ft_calloc(sizeof(char), i + 1);
	else
		str = ft_realloc(str, i + 1);
	MALLOC_NULL_CHECK(str);
	str[++i] = c;
	return (str);
}

char	*ft_get_format(const char *str, va_list arg, t_format *f)
{
	int		i;
	char	*format;

	format = ft_set_format_str(NULL, '%', 1);
	MALLOC_NULL_CHECK(format);
	i = 1;
	ft_reset(f);
	while (str[i])
	{
		if (!ft_set_format(str[i], arg, f))
		{	
			free(format);
			return (NULL);
		}
		format = ft_set_format_str(format, str[i], i + 1);
		if (!str)
			return (NULL);
		if (f->specifier)
			return (format);
		i++;
	}
	free(format);
	return (NULL);
}
