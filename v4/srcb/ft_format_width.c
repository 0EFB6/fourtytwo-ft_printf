/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_format_width.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/27 16:32:04 by cwei-she          #+#    #+#             */
/*   Updated: 2023/07/27 17:17:06 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_bonus.h"

static int	ft_fill(char *str, int i, int len, t_format *f)
{
	while (i < f->width - len)
	{
		if (f->zero && (!f->dot || f->precision < 0))
			str[i] = '0';
		else
			str[i] = ' ';
		i++;
	}
	return (i);
}

static char	*ft_align_left(char *str, t_format *f)
{
	char	*ret;
	int		i;
	int		len;

	i = 0;
	len = ft_strlen(str);
	if (len == 0)
		len = 1;
	ret = ft_calloc(f->width + 1, sizeof(char));
	MALLOC_NULL_CHECK(ret);
	if (len == 1 && (f->dot && f->precision == 0) && (*str == '0' || *str == 0))
		ret[i++] = ' ';
	while (i < f->width)
	{
		if (i < len && (f->specifier == 's' && *str == 0))
			ret[i] = ' ';
		else if (i < len)
			ret[i] = str[i];
		else
			ret[i] = ' ';
		i++;
	}
	free(str);
	return (ret);
}

static char	*ft_align_right(char *str, t_format *f)
{
	char	*ret;
	int		i;
	int		j;
	int		len;

	i = 0;
	j = 0;
	ret = ft_calloc(f->width + 1, sizeof(char));
	MALLOC_NULL_CHECK(ret);
	len = ft_strlen(str);
	if (len == 0)
		len = 1;
	if (str[0] == '-' && (f->zero && (!f->dot || f->precision < 0)))
	{
		ret[i++] = str[j++];
		f->width++;
	}
	i = ft_fill(ret, i, len, f);
	if (len == 1 && (f->dot && f->precision == 0) && (*str == '0' || *str == 0))
		ret[i++] = ' ';
	while (j < len)
		ret[i++] = str[j++];
	free(str);
	return (ret);
}

char	*ft_width(char *str, t_format *f)
{
	char	*ret;

	if (f->width <= (int)ft_strlen(str))
		return (str);
	if (f->minus)
		ret = ft_align_left(str, f);
	else
		ret = ft_align_right(str, f);
	MALLOC_NULL_CHECK(ret);
	return (ret);
}
