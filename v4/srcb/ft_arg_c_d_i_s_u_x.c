/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arg_c_d_s_u_x.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/27 12:04:26 by cwei-she          #+#    #+#             */
/*   Updated: 2023/07/27 17:07:39 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_bonus.h"

char	*ft_arg_c(char c)
{
	char	*ret;

	ret = ft_calloc(2, sizeof(char));
	if (!ret)
		return (NULL);
	ret[0] = c;
	return (ret);
}

char	*ft_arg_d_i(int d)
{
	char	*ret;

	ret = ft_itoa(d);
	MALLOC_NULL_CHECK(ret);
	return (ret);
}

char	*ft_arg_s(char *str)
{
	char	*ret;

	if (!str)
	{
		ret = ft_calloc(ft_strlen(str) + 1, sizeof(char));
		MALLOC_NULL_CHECK(ret);
		ret = ft_memcpy(ret, str, ft_strlen(str));
	}
	else
	{
		ret = ft_calloc(S_EMPTY_LEN + 1, sizeof(char));
		MALLOC_NULL_CHECK(ret);
		ret = ft_memcpy(ret, S_EMPTY, S_EMPTY_LEN);
	}
	return (ret);
}

char	*ft_arg_u(unsigned int nbr)
{
	char	*ret;

	ret = ft_uitoa(nbr);
	MALLOC_NULL_CHECK(ret);
	return (ret);
}

char	*ft_arg_x(unsigned int nbr, int is_upper)
{
	char	*ret;

	ret = ft_itoh(nbr, is_upper);
	MALLOC_NULL_CHECK(ret);
	return (ret);
}
