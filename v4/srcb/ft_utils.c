/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utils.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/26 15:06:39 by cwei-she          #+#    #+#             */
/*   Updated: 2023/07/27 15:38:22 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_bonus.h"

/* Write out characters to standard output recursively based on the n number */
/* given																	 */
int	ft_putnchar_fd(char c, int fd, int n)
{
	int	i;

	i = 0;
	while (n-- > 0)
		i += (int)write(fd, &c, 1);
	return (i);
}

void	ft_reset(t_format *f)
{
	f->minus = 0;
	f->plus = 0;
	f->space = 0;
	f->zero = 0;
	f->hash = 0;
	f->dot = 0;
	f->width = 0;
	f->precision = 0;
	f->specifier = 0;
}

char	*ft_strrev(char *str)
{
	int		i;
	int		j;
	char	tmp;

	i = 0;
	j = ft_strlen(str) - 1;
	while (i < j)
	{
		tmp = str[j];
		str[j] = str[i];
		str[i] = tmp;
		i++;
		j--;
	}
	return (str);
}

void	*ft_realloc(void *ptr, size_t size)
{
	void	*ret;
	size_t	memsize;
	char	*tmp;

	memsize = 0;
	tmp = (char *)ptr;
	while (*tmp++)
		memsize++;
	if (size <= memsize)
		return (ptr);
	ret = malloc(size * sizeof(char));
	MALLOC_NULL_CHECK(ret);
	ft_bzero(ret, size);
	if (!ptr)
		return (ret);
	ft_memcpy(ret, ptr, memsize);
	free(ptr);
	ptr = NULL;
	return (ret);
}
