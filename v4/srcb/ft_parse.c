/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/27 16:15:27 by cwei-she          #+#    #+#             */
/*   Updated: 2023/07/27 23:07:41 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_bonus.h"

char	*ft_parse(va_list arg, t_format *f)
{
	char	*str;
	//char	specifier;

	//specifier = f->specifier;
	if (f->specifier == 'c')
		str = ft_arg_c(va_arg(arg, int));
	else if (f->specifier == 's')
		str = ft_arg_s(va_arg(arg, char *));
	else if (f->specifier == 'p')
		str = ft_arg_p((unsigned long long)va_arg(arg, void *));
	else if (f->specifier == 'd' || f->specifier == 'i')
		str = ft_arg_d_i(va_arg(arg, int));
	else if (f->specifier == 'u')
		str = ft_arg_u(va_arg(arg, int));
	else if (f->specifier == 'x')
		str = ft_arg_x(va_arg(arg, int), 0);
	else if (f->specifier == 'X')
		str = ft_arg_x(va_arg(arg, int), 1);
	else if (f->specifier == '%')
		str = ft_arg_c('%');
	else
		str = ft_calloc(1, sizeof(char));
	MALLOC_NULL_CHECK(str);
	return (str);
}
