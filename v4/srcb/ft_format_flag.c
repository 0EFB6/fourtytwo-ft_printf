/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_format_flag.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/27 16:24:34 by cwei-she          #+#    #+#             */
/*   Updated: 2023/07/27 23:06:23 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_bonus.h"

static char	*ft_format_plus(char *str, t_format *f)
{
	int		i;
	char	*ret;

	if ((f->plus || f->space) && (f->specifier == 'd' || f->specifier == 'i'))
	{
		i = ft_atoi(str);
		if (i < 0)
			return (str);
		ret = ft_calloc(2, sizeof(char));
		MALLOC_NULL_CHECK(ret);
		if (f->plus)
			ret[0] = '+';
		else
			ret[0] = ' ';
		ret = ft_strjoin(ret, str);
		MALLOC_NULL_CHECK(ret);
		free(str);
		return (ret);
	}
	return (str);
}

static char	*ft_format_hash(char *str, t_format *f)
{
	char	*ret;

	if (f->hash && (f->specifier == 'x' || f->specifier == 'X'))
	{
		ret = ft_calloc(3, sizeof(char));
		MALLOC_NULL_CHECK(ret);
		ret[0] = '0';
		if (str[0] != '0')
		{
			ret[1] = f->specifier;
			ret = ft_strjoin(ret, str);
			MALLOC_NULL_CHECK(ret);
		}
		free(str);
		return (ret);
	}
	return (str);
}

char	*ft_flag(char *str, t_format *f)
{
	str = ft_format_plus(str, f);
	MALLOC_NULL_CHECK(str);
	str = ft_format_hash(str, f);
	MALLOC_NULL_CHECK(str);
	return (str);
}
