/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utils_math.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/27 15:19:35 by cwei-she          #+#    #+#             */
/*   Updated: 2023/07/27 17:23:00 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_bonus.h"

static int	ft_nbrlen(unsigned int nbr)
{
	int	count;

	count = 0;
	if (nbr == 0)
		return (1);
	while (nbr != 0)
	{
		nbr /= 10;
		count++;
	}
	return (count);
}

static int	ft_baselen(size_t nbr)
{
	int		i;

	i = 0;
	if (nbr == 0)
		return (1);
	while (nbr > 0)
	{
		nbr /= 16;
		i++;
	}
	return (i);
}

char	*ft_uitoa(unsigned int n)
{
	int				len;
	char			*str;

	len = ft_nbrlen(n);
	str = (char *)malloc(len + 1);
	MALLOC_NULL_CHECK(str);
	str[len] = '\0';
	while (len-- > 0)
	{
		str[len] = n % 10 + '0';
		n /= 10;
	}
	return (str);
}

char	*ft_itoh(size_t nbr, int is_upper)
{
	int		i;
	char	*ret;

	ret = ft_calloc(ft_baselen(nbr) + 1, sizeof(char));
	MALLOC_NULL_CHECK(ret);
	i = 0;
	ret[i] = '0';
	while (nbr > 0)
	{
		if (is_upper)
			ret[i] = HEX_UP[nbr % 16];
		else
			ret[i] = HEX_LOWER[nbr % 16];
		nbr /= 16;
		i++;
	}
	ft_strrev(ret);
	return (ret);
}
