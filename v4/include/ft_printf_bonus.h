/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_bonus.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/08 22:37:02 by cwei-she          #+#    #+#             */
/*   Updated: 2023/07/27 23:23:34 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_BONUS_H
# define FT_PRINTF_BONUS_H
# include "libft.h"
# include <stdarg.h> // va_list, va_arg, va_start, va_end
# include <stdlib.h> // size_t
# include <unistd.h> // write

# define MALLOC_NULL_CHECK(str) if (!str) return NULL
# define INT_MIN -2147483648
# define SPECIFIERS	"cspdiuxX%"
# define HEX_LOWER "0123456789abcdef"
# define HEX_UP "0123456789ABCDEF"
# define S_EMPTY "(null)"
# define S_EMPTY_LEN 6

# ifdef __linux__
#  define IS_LINUX 1
#  define P_EMPTY "(nil)"
#  define P_EMPTY_LEN 5
# else
#  define IS_LINUX 0
#  define P_EMPTY "0x0"
#  define P_EMPTY_LEN 3
# endif

/* Struct to keep track of variables including format specifiers & flags */

typedef struct s_format
{
	int		minus;
	int		plus;
	int		space;
	int		zero;
	int		hash;
	int		dot;
	int		width;
	int		precision;
	char	specifier;
}			t_format;

char		*ft_arg_c(char c);
char		*ft_arg_d_i(int d);
char		*ft_arg_p(size_t str);
char		*ft_arg_s(char *str);
char		*ft_arg_u(unsigned int nbr);
char		*ft_arg_x(unsigned int nbr, int is_upper);
char		*ft_set_format_str(char *str, char c, int i);
char		*ft_get_format(const char *str, va_list arg, t_format *f);
char		*ft_uitoa(unsigned int n);
char		*ft_itoh(size_t nbr, int is_upper);
int			ft_print_char(char *str, int n);
int			ft_print_str(char *str, t_format *f);
int			ft_print_empty(char *str, t_format *f);
int			ft_putnchar_fd(char c, int fd, int n);
void		ft_reset(t_format *f);
char		*ft_strrev(char *str);
void		*ft_realloc(void *ptr, size_t size);
int			ft_printf(const char *str, ...);
char		*ft_flag(char *str, t_format *f);
char		*ft_precision(char *str, t_format *f);
char		*ft_width(char *str, t_format *f);
char		*ft_parse(va_list arg, t_format *f);

#endif