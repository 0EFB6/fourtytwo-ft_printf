/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/08 22:37:02 by cwei-she          #+#    #+#             */
/*   Updated: 2023/07/27 11:45:40 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H
# include "libft.h"
# include <stdarg.h> // va_list, va_arg, va_start, va_end
# include <stdlib.h> // size_t
# include <unistd.h> // write

# define MALLOC_NULL_CHECK(str) if (!str) return NULL
# define INT_MIN -2147483648
# define UINT_MIN 2147483648
# define UINT_MAX 4294967295
# define ULONG_MAX 18446744073709551615UL
# define SPECIFIERS	"cspdiuxX%"
# define HEX_LOWER "0123456789abcdef"
# define HEX_UP "0123456789ABCDEF"

# ifdef __linux__
#  define IS_LINUX 1
#  define OS "LINUX"
#  define S_EMPTY "(null)"
#  define S_EMPTY_LEN 6
#  define P_EMPTY "(nil)"
#  define P_EMPTY_LEN 5
# else
#  define IS_LINUX 0
#  define OS "MAC"
#  define S_EMPTY "(null)"
#  define S_EMPTY_LEN 6
#  define P_EMPTY "0x0"
#  define P_EMPTY_LEN 3
# endif

/* Struct to keep track of variables including format specifiers & flags */

typedef struct s_format
{
	int		minus;
	int		plus;
	int		width;
	int		precision;
	int		negative_precision;
	int		zero;
	int		dot;
	int		space;
	int		hash;
	char	specifier;
}			t_format;

int			ft_arg_c(t_format s, va_list arg);
int			ft_arg_d_i_u(t_format s, va_list arg);
int			ft_arg_s(t_format s, va_list arg);
int			ft_arg_p(t_format s, va_list arg);
int			ft_arg_x(t_format s, va_list arg);
int			ft_parse(char *str, va_list arg);
int			ft_printf(const char *str, ...);
int			ft_nbrlen_c(long n, int base);
int			ft_putnchar_fd(char c, int fd, int n);
int			ft_putnstr_fd(char *str, int fd, int n);
char		*ft_uitoa(unsigned int n);
t_format	ft_format(void);

#endif