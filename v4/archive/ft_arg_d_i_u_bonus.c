/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arg_d_i_u_bonus.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/15 16:30:08 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/20 15:28:30 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_printf_bonus.h"

static char	plus(t_format f)
{
	if (f.plus)
		return ('+');
	return ('-');
}

static int	wil(t_format f, int negative)
{
	if (negative || f.plus)
		return (ft_putnchar_fd(plus(f), 1, f.zero));
	return (0);
}

/* ft_nbr is the function to handle all formatting and print out accordingly  */
/* The count variable is how many times will a certain character be printed	  */
/* Before assigning value to count, update f.width by reducing if condition	  */
/* is met. 																	  */
/* The first 2 ret += is to print out symbol and ' ' if both ' ' flag and	  */
/* '0' flag is used, or else this happens after checking f.zero				  */
/* The if statement is to check the '0' flag whereas the else if statement	  */
/* executes when ' ' flag is used, width is specified and '0' is not specified*/
/* Then the ret+= print '0' if precision is specified and larger than len	  */
/* (or when f.dot)															  */
/* Then the write statement actually writes out the number.					  */
/* Lastly, execute if statement if '-' flag is detected (Left justified) and  */
/* return the numbers of characters printed									  */
static int	ft_nbr(t_format f, char *nbr, int len, int negative)
{
	int	ret;
	int	count;

	ret = 0;
	f.width -= (f.space && !negative && !f.plus && f.width);
	count = f.width - f.precision - negative - f.plus;
	ret += wil(f, negative);
	ret += ft_putnchar_fd(' ', 1, f.zero && !f.dot && f.space);
	if (!f.minus && f.width > f.precision && f.zero)
		ret += ft_putnchar_fd('0', 1, count);
	else if (!f.minus && f.width > f.precision)
		ret += ft_putnchar_fd(' ', 1, count);
	if (negative || f.plus)
		ret += ft_putnchar_fd(plus(f), 1, !f.zero && !f.intmin);
	else if (f.space && !f.intmin)
		ret += ft_putnchar_fd(' ', 1, !f.zero || f.dot);
	ret += ft_putnchar_fd('0', 1, f.precision - len);
	ret += write(1, nbr, len);
	if (f.minus && f.width > f.precision)
		ret += ft_putnchar_fd(' ', 1, count);
	return (ret);
}

/* return the numbers of characters printed									  */
/* Only numbers, '-', '+', ' ', '.' and '0' affect %d, %i and %u specifiers	  */
/* *nbr is the number after converting from unsigned int or int				  */
/* n is storing the number temporarily and for *= -1 processing				  */
/* len is storing the length of the number (nbrlen)							  */
/* negative is indicating whether the number is less than zero or not		  */
/* First if statement, set f.plus to 0 if the number is negative			  */
/* Second if statement, change number to positive if specifier is not %u	  */
/* Third if else statement, convert number to char using ft_uitoa or ft_itoa  */
/* Set len using ft_strlen, if number is 0 set it to 0						  */
/* Set precision to len if precision is lesser than len to prevent			  */
/* missing printing numbers 												  */
/* Run the ft_nbr for printing number accordint to the format				  */
/* Free memory and return the number of characters printed					  */
int	ft_arg_d_i_u(t_format f, va_list arg)
{
	char	*nbr;
	int		n;
	int		ret;
	int		len;
	int		negative;

	n = va_arg(arg, int);
	negative = (n < 0 && n != INT_MIN && f.specifier != 'u');
	if (negative)
		f.plus = 0;
	if (n == INT_MIN)
		f.intmin = 1;
	if (n < 0 && f.specifier != 'u')
		n *= -1;
	if (n < 0 && f.specifier == 'u')
		nbr = ft_uitoa((unsigned)n);
	else
		nbr = ft_itoa(n);
	len = ft_strlen(nbr);
	if (*nbr == '0' && !f.precision && f.dot)
		len = 0;
	if (f.precision < len)
		f.precision = len;
	ret = ft_nbr(f, nbr, len, negative);
	free(nbr);
	return (ret);
}
