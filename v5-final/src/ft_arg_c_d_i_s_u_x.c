/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arg_c_d_i_s_u_x.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/27 12:04:26 by cwei-she          #+#    #+#             */
/*   Updated: 2023/07/28 23:37:08 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

// Allocate memory for the return string with a size of 2 (character and null).
// Check if the allocation is successful.
// Assign the character to the first index of the return string.
// Return the return string.
char	*ft_specifier_c(char c)
{
	char	*ret;

	ret = ft_calloc(sizeof(char), 2);
	MALLOC_NULL_CHECK(ret);
	ret[0] = c;
	return (ret);
}

// Convert the number to a string using ft_itoa() and assign it to the return
// string. Then, check if return string is null.
// Return the return string.
char	*ft_specifier_d_i(int nbr)
{
	char	*ret;

	ret = ft_itoa(nbr);
	MALLOC_NULL_CHECK(ret);
	return (ret);
}

// Allocate memory for the return string with string size + 1 (null terminate)
// If string is null, allocate a size of 6 (S_EMPTY_LEN) + 1 (null terminate)
// for "(null)" return
// Then, check if the allocation is successful.
// Copy the string to the return string using ft_memcpy() then return it
char	*ft_specifier_s(char *str)
{
	char	*ret;

	if (str)
		ret = ft_calloc(sizeof(char), ft_strlen(str) + 1);
	else
		ret = ft_calloc(sizeof(char), S_EMPTY_LEN + 1);
	MALLOC_NULL_CHECK(ret);
	if (str)
		ret = ft_memcpy(ret, str, ft_strlen(str));
	else
		ret = ft_memcpy(ret, S_EMPTY, S_EMPTY_LEN);
	return (ret);
}

// Convert unsigned number to a string using ft_uitoa() and assign it to the
// return string. Then, check if return string is null.
// Return the return string.
char	*ft_specifier_u(unsigned int nbr)
{
	char	*ret;

	ret = ft_uitoa(nbr);
	MALLOC_NULL_CHECK(ret);
	return (ret);
}

// Allocate memory for the buffer with nbr size in hexadecimal which can
// be calculated using ft_baselen() + 1 (null terminate).
// Check if the allocation is successful.
// Convert unsigned number to hexadecimal using ft_itoh() and assign it to
// the return string.
// If the return string is null after using ft_itoh(), free the buffer and
// return null.
// Return the return string.
char	*ft_specifier_x(unsigned int nbr, int is_upper)
{
	char	*ret;
	char	*buffer;

	buffer = ft_calloc(sizeof(char), ft_baselen(nbr) + 1);
	MALLOC_NULL_CHECK(buffer);
	ret = ft_itoh(nbr, is_upper, buffer);
	if (!ret)
	{
		free(buffer);
		return (NULL);
	}
	return (ret);
}
