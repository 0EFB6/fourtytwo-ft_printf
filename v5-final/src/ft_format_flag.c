/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_format_flag.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/27 16:24:34 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/06 17:50:13 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

// Format the string when the flag '+' is present.
// If the flag '+' is true or the flag ' ' is true and the specifier is 'd' or
// 'i', convert the string to integer by using ft_atoi(), then, check if the
// string is negative, if it is, return the string, else, allocate memory for
// the tmp string with size of 2 using ft_calloc().
// Check if the allocation is successful, if it is, check if the flag '+' is
// true, if it is, set the first character of the tmp string to '+', else,
// set the first character of the tmp string to ' '.
// Join the tmp string with the string using ft_strjoin() and assign it to
// tmp, then, free the tmp string. Check also if the return string is valid,
// if is not, return NULL. Free the string and return the return string.
static char	*ft_format_plus(char *str, t_format *f)
{
	int		i;
	char	*ret;
	char	*tmp;

	if ((f->plus || f->space) && (f->specifier == 'd' || f->specifier == 'i'))
	{
		i = ft_atoi(str);
		if (i < 0)
			return (str);
		tmp = ft_calloc(sizeof(char), 2);
		MALLOC_NULL_CHECK(tmp);
		if (f->plus)
			tmp[0] = '+';
		else
			tmp[0] = ' ';
		ret = ft_strjoin(tmp, str);
		MALLOC_NULL_CHECK(ret);
		free(tmp);
		free(str);
		return (ret);
	}
	return (str);
}

// Format the string when the flag '#' is present.
// If the flag '#' is true and the specifier is 'x' or 'X', allocate memory
// for the return string with size of 3 using ft_calloc().
// Check if the allocation is successful, if it is, set the first character
// of the return string to '0', then, check if the first character of the
// string is '0', if it is, free the string and return the return string,
// else, set the second character to the specifier ('x' or 'X').
// Join the return string with string using ft_strjoin() and assign it to
// tmp, then, free the return string. Check also if the tmp string is valid,
// if is not, return NULL. Assign tmp to return string.
// Free the string and return the return string.
static char	*ft_format_hash(char *str, t_format *f)
{
	char	*ret;
	char	*tmp;

	if (f->hash && (f->specifier == 'x' || f->specifier == 'X'))
	{
		ret = ft_calloc(sizeof(char), 3);
		MALLOC_NULL_CHECK(ret);
		ret[0] = '0';
		if (str[0] != '0')
		{
			ret[1] = f->specifier;
			tmp = ft_strjoin(ret, str);
			MALLOC_NULL_CHECK(tmp);
			free(ret);
			ret = tmp;
		}
		free(str);
		return (ret);
	}
	return (str);
}

// Format the flag '+' and '#', check if the string is NULL, if it is, return
// NULL, else return the formatted string.
char	*ft_flag(char *str, t_format *f)
{
	str = ft_format_plus(str, f);
	MALLOC_NULL_CHECK(str);
	str = ft_format_hash(str, f);
	MALLOC_NULL_CHECK(str);
	return (str);
}
