/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/09 19:57:06 by cwei-she          #+#    #+#             */
/*   Updated: 2023/05/09 19:57:06 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	char	*strend;

	strend = (char *)s + ft_strlen((char *)s);
	while (strend > s && *strend != (char)c)
		strend--;
	if (*strend == (char)c)
		return (strend);
	return (NULL);
}
