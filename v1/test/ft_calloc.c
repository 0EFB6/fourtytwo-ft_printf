/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/09 19:53:47 by cwei-she          #+#    #+#             */
/*   Updated: 2023/05/09 19:53:47 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** The calloc() function shall allocate unused space for an array of
** nelem elements each of whose size in bytes is elsize. The space
** shall be initialized to all bits 0. 
*/

#include "libft.h"
#include <stdint.h>

void	*ft_calloc(size_t nitems, size_t size)
{
	void	*ptr;

	if (nitems == SIZE_MAX || size == SIZE_MAX)
		return (NULL);
	ptr = malloc(nitems * size);
	if (!ptr)
		return (NULL);
	ft_bzero(ptr, (nitems * size));
	return ((void *)ptr);
}
