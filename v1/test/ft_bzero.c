/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bzero.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/09 19:12:49 by cwei-she          #+#    #+#             */
/*   Updated: 2023/05/09 19:12:49 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** The bzero() function writes n zeroed bytes (\0) to the string s.  If n
** is zero, bzero() does nothing.
*/

#include "libft.h"

void	ft_bzero(void *c, size_t n)
{
	size_t			i;
	unsigned char	*tmp;

	i = 0;
	tmp = c;
	while (i < n)
	{
		tmp[i] = 0;
		i++;
	}
}
