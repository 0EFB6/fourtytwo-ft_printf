/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/11 13:27:17 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/14 13:51:41 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_printf_bonus.h"

int	main(void)
{
	int	num;

	//ft_printf("Number: %+i\n", 42);
	//ft_printf("Float: %u\n", 429496739);
	//ft_printf("%+d\n", 2147483647);
	//write(1, "\n\n", 2);
	num = -123;
	//ft_printf("h3e %x\n", num);
	//ft_printf("%#x\n", num);
	//ft_printf("%X\n", num);
	//ft_printf("%#X\n", num);
	//char	word[] = "hello world! nice to meet you!";
	//ft_printf("Test%.9s\n", word);
	//ft_printf("Test% d\n", num);
	//int tmp = printf("Test%010u\n", num);
	int tmp = ft_printf("Test%00X\n", num);
	printf("TMP%d\n", tmp);
	tmp = ft_printf("Test%010X\n", num);
	printf("TMP%d\n", tmp);
	tmp = ft_printf("Test%03X\n", num);
	printf("TMP%d\n", tmp);
	tmp = ft_printf("Test%02X\n", num);
	printf("TMP%d\n", tmp);
	tmp = ft_printf("Test%01X\n", num);
	printf("TMP%d\n", tmp);
	
	return (0);
}
