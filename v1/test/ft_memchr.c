/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/09 19:54:22 by cwei-she          #+#    #+#             */
/*   Updated: 2023/05/09 19:54:22 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** The memchr() function scans the initial n bytes of the memory
** area pointed to by s for the first instance of c.  Both c and the
** bytes of the memory area pointed to by s are interpreted as
** unsigned char.
** The memchr() and memrchr() functions return a pointer to the
** matching byte or NULL if the character does not occur in the
** given memory area.
*/

#include "libft.h"

void	*ft_memchr(const void *str, int c, size_t n)
{
	unsigned char	*ptr;
	unsigned char	chr;
	size_t			i;

	ptr = (unsigned char *)str;
	chr = (unsigned char)c;
	i = 0;
	while (i < n)
	{
		if (*ptr == chr)
			return (ptr);
		ptr++;
		i++;
	}
	return (NULL);
}
