/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/09 19:56:09 by cwei-she          #+#    #+#             */
/*   Updated: 2023/05/16 13:20:09 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	int		s1len;
	int		s2len;
	char	*res;

	s1len = ft_strlen((char *)s1);
	s2len = ft_strlen((char *)s2);
	if (!s1 || !s2)
		return (NULL);
	res = malloc(sizeof(*res) * (s1len + s2len + 1));
	if (!res)
		return (NULL);
	ft_memcpy(res, s1, s1len);
	ft_memcpy(res + s1len, s2, s2len + 1);
	return (res);
}
