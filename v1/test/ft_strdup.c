/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/09 19:56:00 by cwei-she          #+#    #+#             */
/*   Updated: 2023/05/15 16:06:43 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *str)
{
	size_t	i;
	size_t	len;
	char	*dst;

	len = ft_strlen(str);
	dst = malloc(sizeof(*dst) * (len + 1));
	if (dst)
	{
		i = 0;
		while (str[i] != '\0')
		{
			dst[i] = str[i];
			i++;
		}
		dst[i] = '\0';
	}
	return (dst);
}
