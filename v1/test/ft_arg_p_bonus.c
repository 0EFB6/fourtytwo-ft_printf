/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arg_p.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/09 22:55:12 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/09 22:55:12 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_bonus.h"

int	ft_arg_p(unsigned long address)
{
	char	*number;
	int		i;
	int		ret;

	i = 0;
	if (!address)
		return (write(1, "(nil)", 5));
	ret = write(1, "0x", 2);
	number = malloc(sizeof(char) * (ft_hex_len(address) + 1));
	while (address)
	{
		number[i] = ft_dtoh(address % 16, 'x');
		address /= 16;
		i++;
	}
	number[i] = '\0';
	ret += ft_print_strrev(number);
	ft_free(&number);
	return (ret);
}
