/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/09 19:56:21 by cwei-she          #+#    #+#             */
/*   Updated: 2023/05/16 14:21:10 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	len(char *str)
{
	int	count;

	count = 0;
	if (str == NULL)
		return (0);
	while (*str != '\0')
	{
		count++;
		str++;
	}
	return (count);
}

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	char	*s;
	size_t	srclen;
	size_t	dstlen;
	size_t	result;
	size_t	i;

	s = (char *)src;
	srclen = len((char *)src);
	dstlen = len(dst);
	if (size == 0)
		return (srclen);
	result = 0;
	i = 0;
	if (size > dstlen)
		result = srclen + dstlen;
	else
		result = srclen + size;
	while (s[i] && (dstlen + 1) < size)
	{
		dst[dstlen] = s[i];
		dstlen++;
		i++;
	}
	dst[dstlen] = '\0';
	return (result);
}
