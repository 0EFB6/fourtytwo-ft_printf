/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arg_d_i.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/09 21:28:02 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/14 12:36:55 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_bonus.h"
#include <stdio.h>

static void	ft_putnbr(int nb)
{
	if (nb == -2147483648)
	{
		write(1, "-", 1);
		write(1, "2", 1);
		ft_putnbr(147483648);
	}
	else if (nb < 0)
	{
		write(1, "-", 1);
		nb *= -1;
		ft_putnbr(nb);
	}
	else if (nb > 9)
	{
		ft_putnbr(nb / 10);
		ft_putnbr(nb % 10);
	}
	else
		ft_putchar_fd(48 + nb, 1);
}

static int	ft_nbrlen(int i)
{
	int	count;

	if (i == 0)
		return (2);
	count = 0;
	if (i < 0)
	{
		i /= 10;
		count += 2;
		i = -i;
	}
	while (i > 0)
	{
		i /= 10;
		count++;
	}
	return (count);
}

int	ft_arg_d_i(int n, int len, char flag)
{
	int	i;
	int	zero;

	i = 0;
	if ((flag == '+' || flag == ' ') && n >= 0)
	{
		ft_putchar_fd(flag, 1);
		ft_putnbr(n);
		return (ft_decimal_len(n) + 1);
	}
	else if (flag == '0' && len > ft_nbrlen(n))
	{
		zero = len - ft_nbrlen(n);
		if (n > 0)
			while (i++ < zero)
				ft_putchar_fd('0', 1);
		else if (n < 0)
		{
			ft_putchar_fd('-', 1);
			while (i++ < zero)
				ft_putchar_fd('0', 1);
			n = -n;
		}
		else
			while (i++ <= zero)
				ft_putchar_fd('0', 1);
		ft_putnbr(n);
		return (len);
	}
	else
	{
		ft_putnbr(n);
		return (ft_decimal_len(n));
	}
}
