/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putendl_fd.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/09 19:55:39 by cwei-she          #+#    #+#             */
/*   Updated: 2023/05/15 16:06:22 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Outputs the string 's' to the given file descriptor followed
** by a new line.
*/

#include "libft.h"

void	ft_putendl_fd(char *s, int fd)
{
	ft_putstr_fd(s, fd);
	ft_putchar_fd('\n', fd);
}
