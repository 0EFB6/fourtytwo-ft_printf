/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arg_u.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/09 21:08:28 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/14 12:15:50 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_printf_bonus.h"

void	ft_putchar_fd_u(char c, int fd, int *count)
{
	write(fd, &c, 1);
	(*count)++;
}

static	void	ft_putnbr_u(unsigned int n, int *count)
{
	if (n < 10)
		ft_putchar_fd_u(n + '0', 1, count);
	else
	{
		ft_putnbr_u(n / 10, count);
		ft_putnbr_u(n % 10, count);
	}
}

static int	ft_nbrlen(int i)
{
	int	count;

	if (i < 0)
		i = -i;
	if (i == 0)
		return (2);
	count = 0;
	if (i < 0)
	{
		i /= 10;
		count += 2;
		i = -i;
	}
	while (i > 0)
	{
		i /= 10;
		count++;
	}
	return (count);
}

int	ft_arg_u(unsigned int n, int len, char flag)
{
	int		i;
	int		tmp;
	char	*str;

	i = 0;
	tmp = 0;
	str = ft_itoa(n);
	if (flag == '0')
	{
		if (str[0] == '-')
		{
			if (len > 10)
			{
				while (i++ < (len - 10))
					ft_putchar_fd('0', 1);
				i--;
			}
			ft_putnbr_u(n, &tmp);
			if (len == 0)
				return (ft_decimal_len(n));
			return (tmp + i);
		}
		if (n > 0 && !(n < 0))
			while (i++ < (len - ft_nbrlen(n)))
				ft_putchar_fd('0', 1);
		else if (n == 0)
			while (i++ <= (len - ft_nbrlen(n)))
				ft_putchar_fd('0', 1);
		ft_putnbr_u(n, &tmp);
		i--;
		return (ft_decimal_len(n) + i);
	}
	ft_putnbr_u(n, &tmp);
	return (ft_decimal_len(n));
}
