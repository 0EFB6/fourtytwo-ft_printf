/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/09 19:54:46 by cwei-she          #+#    #+#             */
/*   Updated: 2023/05/09 19:54:46 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** The memcmp() function compares the first n bytes (each
** interpreted as unsigned char) of the memory areas s1 and s2.
** The memcmp() function returns an integer less than, equal to, or
** greater than zero if the first n bytes of s1 is found,
** respectively, to be less than, to match, or be greater than the
** first n bytes of s2.
*/

#include "libft.h"

int	ft_memcmp(const void *s1, const void *s2, size_t n)
{
	unsigned char	*s1str;
	unsigned char	*s2str;
	size_t			i;

	s1str = (unsigned char *)s1;
	s2str = (unsigned char *)s2;
	i = 0;
	while (i < n)
	{
		if (s1str[i] != s2str[i])
			return (s1str[i] - s2str[i]);
		i++;
	}
	return (0);
}
