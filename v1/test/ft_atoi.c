/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/09 19:53:37 by cwei-she          #+#    #+#             */
/*   Updated: 2023/05/09 19:53:37 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/* 
** The atoi() function converts the initial portion of the string
** pointed to by nptr to int. It does not detect errors.
*/

#include "libft.h"

int	ft_atoi(const char *s)
{
	int	sign;
	int	res;
	int	i;

	res = 0;
	i = 0;
	while ((s[i] > 8 && s[i] < 20) || s[i] == ' ')
		i++;
	if (s[i] == '-')
		sign = -1;
	else
		sign = 1;
	if (s[i] == '+' || s[i] == '-')
		i++;
	while ((s[i] >= '0' && s[i] <= '9') && s[i] != '\0')
		res = res * 10 + (s[i++] - '0');
	return (res * sign);
}
