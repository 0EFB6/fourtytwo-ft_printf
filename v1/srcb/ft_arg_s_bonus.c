/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arg_s.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/09 21:03:46 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/13 16:50:53 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_printf_bonus.h"

int	ft_arg_s(char *str, int len, char flag)
{
	int	i;

	i = 0;
	if (!str)
		return (write(1, "(null)", 6));
	//printf("RETT%s\n\n\n", str);
	if (len != 0 && flag == '.')
	{
		while (i < len && str[i] != '\0')
		{
			ft_putchar_fd(str[i], 1);
			i++;
		}
		return (len);
	}
	else if (len == 0 && flag == '.')
		return (0);
	else
		ft_putstr_fd(str, 1);
	return (ft_strlen(str));
}
