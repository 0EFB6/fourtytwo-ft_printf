/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arg_x.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/09 22:47:19 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/14 13:49:49 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_printf_bonus.h"

static int	ft_arg_x_zero(int len)
{
	int	i;

	i = 1;
	if (len > 1)
	{
		while (i++ < (len + 1))
			ft_putchar_fd('0', 1);
		return (i - 2);
	}
	ft_putchar_fd('0', 1);
	return (i);
}

int	ft_arg_x(unsigned int nbr, char type, char flag, int len)
{
	char	*number;
	int		i;
	int		ret;

	i = 0;
	ret = 0;
	//printf("\nGG%d\n", ret);
	if (!nbr)
		return (ft_arg_x_zero(len));
	number = malloc(sizeof(char) * (ft_hex_len(nbr) + 1));
	while (nbr)
	{
		number[i] = ft_dtoh(nbr % 16, type);
		nbr /= 16;
		i++;
	}
	number[i] = '\0';
	if (flag == '0' && len > (int)ft_strlen(number))
	{
		i = 0;
		while (i++ < (len - (int)ft_strlen(number)))
			ft_putchar_fd('0', 1);
		ret += (len - ft_strlen(number));
	}
	if (flag == '#')
	{
		ft_putchar_fd('0', 1);
		ft_putchar_fd(type, 1);
	}
	ret += ft_print_strrev(number);
	//printf("\nGG%d\n", ret);
	ft_free(&number);
	return (ret);
}
