/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_bonus.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/09 23:00:28 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/14 13:43:39 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_printf_bonus.h"

static int	ft_check_arg(va_list arg, char c, char flag)
{
	int	ret;

	ret = 0;
	if (c == '%')
		ret += ft_arg_percent();
	if (c == 'c')
		ret += ft_arg_c(va_arg(arg, int));
	if (c == 'd' || c == 'i')
		ret += ft_arg_d_i(va_arg(arg, int), 0, flag);
	if (c == 'p')
		ret += ft_arg_p(va_arg(arg, unsigned long));
	if (c == 's')
		ret += ft_arg_s(va_arg(arg, char *), 0, '$');
	if (c == 'u')
		ret += ft_arg_u(va_arg(arg, int), 0, flag);
	if (c == 'x' || c == 'X')
		ret += ft_arg_x(va_arg(arg, unsigned int), c, flag, 0);
	return (ret);
}

static char	ft_check_flags(char c)
{
	char	flag;

	flag = 0;
	if (c == '#')
		flag |= c;
	if (c == ' ')
		flag |= c;
	if (c == '+')
		flag |= c;
	if (c == '-')
		flag |= c;
	if (c == '.')
		flag |= c;
	if (c == '0')
		flag |= c;
	return (flag);
}

// static int	ft_width(char *str, va_list arg)
// {
// 	int	is_switch_off;
// 	int	minus;
// 	int	zero;
// 
// 	is_switch_off = 0;
// 	while (*str != '.' && !ft_strchr(SPECIFIERS, *str))
// 	{
// 		if (*str == '-')
// 		{
// 			minus = 1;
// 			return (minus);
// 		}
// 		if (*str == '0' && !ft_isdigit(*(str - 1)))
// 		{
// 			zero = 1;
// 			return (zero);
// 		}
// 		else if (((*str > '0' && *str < '9') || *str == '*') && !is_switch_off)
// 		{
// 			if (*str == '*')
// 				return (va_arg(arg, int));
// 			else
// 				return (ft_atoi(str));
// 			is_switch_off = 1;
// 		}
// 		str++;
// 	}
// 	return (0);
// }
// 
// static int	ft_precision(char *str, va_list arg)
// {
// 	int	is_switch_off;
// 	int	ret;
// 
// 	is_switch_off = 0;
// 	while (!ft_strchr(SPECIFIERS, *str))
// 	{
// 		if ((ft_isdigit(*str) || *str == '*') && !is_switch_off)
// 		{
// 			if (*str == '*')
// 				ret = va_arg(arg, int);
// 			else
// 			{
// 				ret = ft_atoi(str);
// 				printf("%d", ret);
// 			}
// 			is_switch_off = 1;
// 		}
// 		printf("str: %c\n", *str);
// 		str++;
// 	}
// 	return (ret);
// }

static int	ft_nbrlen(int i)
{
	int	count;

	if (i == 0)
		return (1);
	count = 0;
	if (i < 0)
	{
		i /= 10;
		count += 2;
		i = -i;
	}
	while (i > 0)
	{
		i /= 10;
		count++;
	}
	return (count);
}

static int	ft_check_arg_bonus(va_list arg, char *str, char flag)
{
	int	ret;
	int	is_switch_off;
	int	len;

	ret = 0;
	is_switch_off = 0;
	len = 0;
	//printf("RET%c\n\n", *(str + 1));
	//printf("%s\n\n", str);
	while (!ft_strchr(SPECIFIERS, *str))
	{
		if ((*str >= '0' && *str <= '9') && ft_isdigit(*str))
			len = ft_atoi(str);
		str = str + ft_nbrlen(len);
	}
	if ((*str == 'd' || *str == 'i' || *str == 'u'
			|| *str == 'x' || *str == 'X') && flag == '0')
	{
		//printf("RET%c\n\n", *str);
		if (*str == 'u')
			ret += ft_arg_u(va_arg(arg, int), len, flag);
		if (*str == 'd' || *str == 'i')
			ret += ft_arg_d_i(va_arg(arg, int), len, flag);
		if (*str == 'x' || *str == 'X')
			ret += ft_arg_x(va_arg(arg, unsigned int), *str, flag, len);
	}
	if (*str == 's' && flag == '.')
		ret += ft_arg_s(va_arg(arg, char *), len, flag);
	
	//printf("\nBONUS%d\n\n", ret);
	return (ret);
}

static int	ft_nbr_checker(char *str)
{
	int ret;

	ret = 0;
	//printf("\n\nSTR%s\n", str);
	while (!ft_strchr(SPECIFIERS, *str))
	{
		//printf("LOOP%c\n", *str);
		if ((*str >= '0' && *str <= '9') && ft_isdigit(*str))
			ret++;
		str++;
	}
	//printf("NBR%d\n", ret);
	return (ret);
}

int	ft_printf(const char *str, ...)
{
	int		i;
	int		ret;
	char	c;
	int		len;
	va_list	arg;
	// int		tmp;

	i = 0;
	ret = 0;
	va_start(arg, str);
	while (*str)
	{
		c = *(str + 1);
		//printf("DEBUG%s\n\n", str);
		if (*str == '%')
		{
			// tmp = ft_width((char *)str, arg);
			// printf("tmp: %d\n", tmp);
			if (c == '#' || c == ' ' || c == '+')
			{
				ret += ft_check_arg(arg, *(str + 2), ft_check_flags(c));
				str = str + 2;
			}
			if (c == '-' || c == '.')
			{
				ret += ft_check_arg_bonus(arg, (char *)(str + 2), ft_check_flags(c));
				len = ft_nbr_checker((char *)(str + 2));
				str = str + 2 + len;
			}
			if (c == '0')
			{
				ret += ft_check_arg_bonus(arg, (char *)(str + 2), ft_check_flags(c));
				len = ft_nbr_checker((char *)(str + 2));
				str = str + 2 + len;
			}
			ret += ft_check_arg(arg, c, ft_check_flags(c));
			//str++;
		}
		else
			ret += write(1, &*str, 1);
		str++;
	}
	va_end(arg);
	//printf("\n\nRETT%d\n\n", ret);
	return (ret);
}
