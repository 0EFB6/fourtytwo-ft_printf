/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/08 22:37:02 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/10 00:28:45 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H
# include "libft.h"
# include <stdarg.h>
# include <stdlib.h>
# include <unistd.h>

int		ft_arg_c(char c);
int		ft_arg_d_i(int n);
int		ft_arg_p(unsigned long address);
int		ft_arg_percent(void);
int		ft_arg_s(char *str);
int		ft_arg_u(unsigned int n);
int		ft_arg_x(unsigned int nbr, char type);
int		ft_decimal_len(long int n);
char	ft_dtoh(char digit, char type);
void	ft_free(char **str);
int		ft_hex_len(unsigned long n);
int		ft_print_strrev(char *str);
int		ft_printf(const char *str, ...);

#endif