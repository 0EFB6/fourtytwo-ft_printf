/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dtoh.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/09 22:24:59 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/09 22:24:59 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	ft_dtoh(char digit, char type)
{
	int	is_upper;

	is_upper = 0;
	if (type == 'X')
		is_upper = 32;
	if (digit > 9)
	{
		if (digit == 10)
			return ('a' - is_upper);
		if (digit == 11)
			return ('b' - is_upper);
		if (digit == 12)
			return ('c' - is_upper);
		if (digit == 13)
			return ('d' - is_upper);
		if (digit == 14)
			return ('e' - is_upper);
		if (digit == 15)
			return ('f' - is_upper);
	}
	else
		return (digit + '0');
	return (0);
}
