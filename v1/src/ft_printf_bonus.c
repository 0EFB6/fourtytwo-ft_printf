/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/09 23:00:28 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/09 23:00:28 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_bonus.h"

static int	ft_check_arg(va_list arg, char c)
{
	int	ret;

	ret = 0;
	if (c == '%')
		ret += ft_arg_percent();
	if (c == 'c')
		ret += ft_arg_c(va_arg(arg, int));
	if (c == 'd' || c == 'i')
		ret += ft_arg_d_i(va_arg(arg, int));
	if (c == 'p')
		ret += ft_arg_p(va_arg(arg, unsigned long));
	if (c == 's')
		ret += ft_arg_s(va_arg(arg, char *));
	if (c == 'u')
		ret += ft_arg_u(va_arg(arg, int));
	if (c == 'x' || c == 'X')
		ret += ft_arg_x(va_arg(arg, unsigned int), c);
	return (ret);
}

static char	ft_check_flags(char c)
{
	char	flag;

	flag = 0;
	while (c)
	{
		if (c == '#')
			flag |= c;
		if (c == ' ')
			flag != c;
		if (c == '+')
			flag != c;
	}
	return (flag);
}

int	ft_printf(const char *str, ...)
{
	int		i;
	int		ret;
	char	c;
	char	flag;
	va_list	arg;

	i = 0;
	ret = 0;
	va_start(arg, str);
	while (str[i])
	{
		c = str[i + 1];
		if (str[i] == '%')
		{
			ret += ft_check_arg(arg, c);
			i++;
		}
		else
			ret += write(1, &str[i], 1);
		i++;
	}
	va_end(arg);
	return (ret);
}
