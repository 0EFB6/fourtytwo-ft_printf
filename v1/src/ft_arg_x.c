/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arg_x.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/09 22:47:19 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/09 22:47:19 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_arg_x(unsigned int nbr, char type)
{
	char	*number;
	int		i;
	int		ret;

	i = 0;
	if (!nbr)
		return (write(1, "0", 1));
	number = malloc(sizeof(char) * (ft_hex_len(nbr) + 1));
	while (nbr)
	{
		number[i] = ft_dtoh(nbr % 16, type);
		nbr /= 16;
		i++;
	}
	number[i] = '\0';
	ret = ft_print_strrev(number);
	ft_free(&number);
	return (ret);
}
