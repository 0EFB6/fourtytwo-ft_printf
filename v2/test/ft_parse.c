/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/15 12:10:57 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/16 18:20:34 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_printf.h"

static t_format	ft_parse_bonus(char *str, t_format f)
{
	while (*str != '.' && !ft_strchr(SPECIFIERS, *str))
	{
		if (*str == '+')
			f.plus = 1;
		if (*str == ' ')
			f.space = 1;
		if (*str == '#')
			f.hash = 1;
		str++;
	}
	return (f);
}

static t_format	ft_parse_width(char *str, va_list arg, t_format f)
{
	int	is_switch_off;

	is_switch_off = 0;
	while (*str != '.' && !ft_strchr(SPECIFIERS, *str))
	{
		if (*str == '-')
			f.minus = 1;
		if (*str == '0' && !ft_isdigit(*(str - 1)))
			f.zero = 1;
		else if (((*str > '0' && *str <= '9') || *str == '*') && !is_switch_off)
		{
			if (*str == '*')
				f.width = va_arg(arg, int);
			else
				f.width = ft_atoi(str);
			is_switch_off = 1;
		}
		str++;
	}
	return (f);
}

static t_format	ft_parse_precision(char *str, va_list arg, t_format f)
{
	int	is_switch_off;

	is_switch_off = 0;
	while (!ft_strchr(SPECIFIERS, *str))
	{
		if ((ft_isdigit(*str) || *str == '*') && !is_switch_off)
		{
			if (*str == '*')
				f.precision = va_arg(arg, int);
			else
				f.precision = ft_atoi(str);
			is_switch_off = 1;
		}
		str++;
	}
	return (f);
}

int	ft_parse(char *str, va_list arg)
{
	t_format	format;

	format = ft_parse_width(str, arg, ft_format());
	format = ft_parse_bonus(str, format);
	while (!ft_strchr(SPECIFIERS, *str) && *str != '.')
		str++;
	if (*str == '.' && !format.specifier)
	{
		format.dot = 1;
		format = ft_parse_precision(str++, arg, format);
		while (!ft_strchr(SPECIFIERS, *str))
			str++;
	}
	if (format.width < 0)
	{
		format.minus = 1;
		format.width *= -1;
	}
	format.specifier = *str;
	format.negative_precision = format.precision < 0;
	return (ft_arg_check(format, arg));
}
