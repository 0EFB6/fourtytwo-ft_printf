/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnchar_fd.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/15 15:39:06 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/17 00:11:33 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_putnchar_fd(char c, int fd, int n)
{
	int	i;

	i = 0;
	while (n-- > 0)
		i += (int)write(fd, &c, 1);
	return (i);
}
