/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test-x.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/16 00:44:15 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/16 13:51:55 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_printf.h"

int	main(void)
{
	int	tmp;

	tmp = ft_printf("Unsigned hexadecimal: %x\n", 42);
	printf("Return value for ft_printf: %d\n", tmp);
	tmp = ft_printf("Unsigned hexadecimal: %x\n", -42);
	printf("Return value for ft_printf: %d\n", tmp);
	tmp = ft_printf("Unsigned hexadecimal: %x\n", 0);
	printf("Return value for ft_printf: %d\n", tmp);
	tmp = ft_printf("Unsigned hexadecimal: %x\n", 255);
	printf("Return value for ft_printf: %d\n", tmp);
	tmp = ft_printf("Unsigned hexadecimal: %x\n", 65535);
	printf("Return value for ft_printf: %d\n", tmp);
	tmp = ft_printf("Unsigned hexadecimal: %x\n", 4294967295);
	printf("Return value for ft_printf: %d\n", tmp);
	tmp = ft_printf("Unsigned hexadecimal: %x\n", -4294967295);
	printf("Return value for ft_printf: %d\n", tmp);
	tmp = ft_printf("Unsigned hexadecimal: %x\n", 123456789);
	printf("Return value for ft_printf: %d\n", tmp);
	tmp = ft_printf("Unsigned hexadecimal: %x\n", -123456789);
	printf("Return value for ft_printf: %d\n", tmp);
	tmp = ft_printf("Unsigned hexadecimal: %x\n", -2147483648);
	printf("Return value for ft_printf: %d\n", tmp);
	return (0);
}
