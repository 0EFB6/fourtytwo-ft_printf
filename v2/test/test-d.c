/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test-d.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/15 23:04:48 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/16 11:45:17 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_printf.h"

int	main(void)
{
	int	num;
	int	tmp;

	num = 42;
	tmp = ft_printf("%d\n", num);
	printf("Return value for ft_printf: %d\n", tmp);
	tmp = ft_printf("%d\n", -num);
	printf("Return value for ft_printf: %d\n", tmp);
	tmp = ft_printf("%d\n", 0);
	printf("Return value for ft_printf: %d\n", tmp);
	tmp = ft_printf("%d\n", 1234567890);
	printf("Return value for ft_printf: %d\n", tmp);
	tmp = ft_printf("%d\n", -1234567890);
	printf("Return value for ft_printf: %d\n", tmp);
	tmp = ft_printf("%.2d\n", num);
	printf("Return value for ft_printf: %d\n", tmp);
	return (0);
}
