/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arg_p.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/15 17:15:43 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/15 22:54:30 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	ft_nbrlen(long n, int base)
{
	int	count;

	if (n == 0)
		return (1);
	count = 0;
	if (!base)
		base = 10;
	while (n != 0)
	{
		n /= base;
		count++;
	}
	return (count);
}

static int	ft_hex(t_format f, size_t n, size_t iteration)
{
	int		ret;
	int		remainder;
	char	c;

	ret = 0;
	if (n > 0 || (!iteration && (f.specifier != 'p' || !f.dot)))
	{
		remainder = n % 16;
		if (f.specifier != 'X')
			c = HEX_LOWER[remainder];
		else
			c = HEX_UP[remainder];
		n /= 16;
		iteration = 1;
		ret += ft_hex(f, n, iteration);
		ret += ft_putnchar_fd(c, 1, 1);
	}
	return (ret);
}

int	ft_arg_p(t_format f, va_list arg)
{
	size_t	n;
	int		ret;
	int		len;

	n = va_arg(arg, size_t);
	ret = 0;
	len = ft_nbrlen(n, 16);
	len *= !(!n && f.precision && f.dot);
	if (n == 0)
		return (write(1, "(nil)", 5));
	if (f.precision < len || f.dot)
		f.precision = len;
	ret += write(1, "0x", 2 * f.zero);
	f.width -= 2;
	if (!f.minus && f.width > f.precision && !f.dot && f.zero)
		ret += ft_putnchar_fd('0', 1, f.width - f.precision);
	else if (!f.minus && f.width > f.precision)
		ret += ft_putnchar_fd(' ', 1, f.width - f.precision);
	ret += write(1, "0x", 2 * !f.zero);
	ret += ft_putnchar_fd('0', 1, (f.precision - len) * (n != 0));
	ret += ft_putnchar_fd('0', 1, f.precision * (f.dot && !n));
	if (len)
		ret += ft_hex(f, n, n);
	if (f.minus && f.width > f.precision)
		ret += ft_putnchar_fd(' ', 1, f.width - f.precision);
	return (ret);
}
