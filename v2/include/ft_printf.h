/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/08 22:37:02 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/16 18:08:53 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H
# include "libft.h"
# include <stdarg.h>
# include <stdlib.h>
# include <unistd.h>

# define MALLOC_NULL_CHECK(str) if (!str) return NULL
# define INT_MIN -2147483648
# define UINT_MAX 4294967295
# define SPECIFIERS	"cspdiuxX%"
# define HEX_LOWER "0123456789abcdef"
# define HEX_UP "0123456789ABCDEF"

/* Struct to keep track of variables including format specifiers & flags */

typedef struct s_format
{
	int		minus;
	int		plus;
	int		width;
	int		precision;
	int		negative_precision;
	int		zero;
	int		dot;
	int		space;
	int		hash;
	char	specifier;
}			t_format;

int			ft_printf(const char *str, ...);
t_format	ft_format(void);
int			ft_arg_check(t_format s, va_list arg);
int			ft_arg_c(t_format s, va_list arg);
int			ft_arg_s(t_format s, va_list arg);
int			ft_arg_d_i_u(t_format s, va_list arg);
int			ft_arg_p(t_format s, va_list arg);
int			ft_arg_x(t_format s, va_list arg);
int			ft_parse(char *str, va_list arg);
int			ft_putnchar_fd(char c, int fd, int n);
int			ft_putnstr_fd(char *str, int fd, int n);
char		*ft_uitoa(unsigned int n);

#endif