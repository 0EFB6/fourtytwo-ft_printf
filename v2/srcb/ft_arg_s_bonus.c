/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arg_s.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/15 15:59:21 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/15 22:32:54 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_arg_s(t_format f, va_list arg)
{
	int		ret;
	char	*str;

	ret = 0;
	str = va_arg(arg, char *);
	if (!str)
		str = "(null)";
	if (!f.dot || f.precision > (int)ft_strlen(str) || f.precision < 0)
		f.precision = ft_strlen(str);
	if (!f.minus && f.width > f.precision
		&& f.zero && (!f.dot || f.negative_precision))
		ret += ft_putnchar_fd('0', 1, f.width - f.precision);
	else if (!f.minus && f.width - f.precision > 0)
		ret += ft_putnchar_fd(' ', 1, f.width - f.precision);
	ret += ft_putnstr_fd(str, 1, f.precision);
	if (f.minus && f.width - f.precision > 0)
		ret += ft_putnchar_fd(' ', 1, f.width - f.precision);
	return (ret);
}
