/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arg_c.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/15 15:35:17 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/17 00:34:49 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_arg_c(t_format f, va_list arg)
{
	char	c;
	int		ret;

	ret = 0;
	if (f.specifier == 'c')
		c = va_arg(arg, int);
	else
		c = '%';
	//f.precision = 1;
	//if (!f.minus && f.zero)
	//	ret += ft_putnchar_fd('0', 1, f.width - f.precision);
	//else if (!f.minus && f.width > f.precision)
	//	ret += ft_putnchar_fd(' ', 1, f.width - f.precision);
	ret += ft_putnchar_fd(c, 1, 1);
	//if (f.minus && f.width - f.precision > 0)
	//	ret += ft_putnchar_fd(' ', 1, f.width - f.precision);
	return (ret);
}
