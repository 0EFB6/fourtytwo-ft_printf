/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/15 12:10:57 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/17 01:09:46 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_parse(char *str, va_list arg)
{
	t_format	format;

	format = ft_format();
	format.specifier = *str;
	return (ft_arg_check(format, arg));
}
