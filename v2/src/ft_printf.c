/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/15 11:34:35 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/15 22:25:16 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_arg_check(t_format f, va_list arg)
{
	int	ret;

	ret = 0;
	if (f.specifier == 'c' || f.specifier == '%')
		ret = ft_arg_c(f, arg);
	if (f.specifier == 's')
		ret = ft_arg_s(f, arg);
	if (f.specifier == 'd' || f.specifier == 'i' || f.specifier == 'u')
		ret = ft_arg_d_i_u(f, arg);
	if (f.specifier == 'x' || f.specifier == 'X')
		ret = ft_arg_x(f, arg);
	if (f.specifier == 'p')
		ret = ft_arg_p(f, arg);
	return (ret);
}

int	ft_printf(const char *str, ...)
{
	int		ret;
	char	*c;
	va_list	arg;

	ret = 0;
	va_start(arg, str);
	while (*str)
	{
		if (*str == '%')
		{
			c = (char *)str;
			if (*(++str))
				ret += ft_parse((char *)str, arg);
			while (*str && !ft_strchr(SPECIFIERS, *str))
				str++;
			if (!(*str))
				str = c;
		}
		else
			ret += ft_putnchar_fd(*str, 1, 1);
		if (*str)
			str++;
	}
	va_end(arg);
	return (ret);
}
