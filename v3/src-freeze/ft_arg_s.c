/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arg_s.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/15 15:59:21 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/17 22:57:05 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_arg_s(t_format f, va_list arg)
{
	int		ret;
	char	*str;

	ret = 0;
	str = va_arg(arg, char *);
	if (!str)
		str = "(null)";
	if (!f.dot || f.precision > (int)ft_strlen(str) || f.precision < 0)
		f.precision = ft_strlen(str);
	ret += ft_putnstr_fd(str, 1, f.precision);
	return (ret);
}
