/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arg_d_i_u.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/15 16:30:08 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/17 22:56:43 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static char	plus(t_format f)
{
	if (f.plus)
		return ('+');
	return ('-');
}

static int	ft_nbr(t_format f, char *nbr, int len, int negative)
{
	int	ret;

	ret = 0;
	if (negative || f.plus)
		ret += ft_putnchar_fd(plus(f), 1, f.zero
				&& (!f.dot || f.negative_precision));
	if (negative || f.plus)
		ret += ft_putnchar_fd(plus(f), 1, !f.zero
				|| (f.dot && !f.negative_precision));
	ret += write(1, nbr, len);
	return (ret);
}

int	ft_arg_d_i_u(t_format f, va_list arg)
{
	char	*nbr;
	int		n;
	int		ret;
	int		len;
	int		negative;

	n = va_arg(arg, int);
	ret = 0;
	negative = (n < 0 && n != INT_MIN && f.specifier != 'u');
	if (negative)
		f.plus = 0;
	if (n < 0 && f.specifier != 'u')
		n *= -1;
	if (n < 0 && f.specifier == 'u')
		nbr = ft_uitoa((unsigned)n);
	else
		nbr = ft_itoa(n);
	len = ft_strlen(nbr);
	ret += ft_nbr(f, nbr, len, negative);
	free(nbr);
	return (ret);
}
