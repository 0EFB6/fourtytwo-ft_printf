/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arg_x.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/15 17:15:26 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/18 13:29:33 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static char	*ft_flag(t_format f)
{
	if (f.specifier == 'X')
		return ("0X");
	return ("0x");
}

static int	ft_hex(t_format f, size_t n, size_t iteration)
{
	int		ret;
	int		remainder;
	char	c;

	ret = 0;
	if (n > 0 || (!iteration && (f.specifier != 'p' || !f.dot)))
	{
		remainder = n % 16;
		if (f.specifier != 'X')
			c = HEX_LOWER[remainder];
		else
			c = HEX_UP[remainder];
		n /= 16;
		iteration = 1;
		ret += ft_hex(f, n, iteration);
		ret += ft_putnchar_fd(c, 1, 1);
	}
	return (ret);
}

static int	ft_nbrlen(long n, int base)
{
	int	count;

	if (n == 0)
		return (1);
	count = 0;
	if (!base)
		base = 10;
	while (n != 0)
	{
		n /= base;
		count++;
	}
	return (count);
}

int	ft_arg_x(t_format f, va_list arg)
{
	unsigned int	n;
	int				len;
	int				ret;

	n = va_arg(arg, unsigned int);
	len = ft_nbrlen(n, 16);
	ret = 0;
	ret += ft_putnstr_fd(ft_flag(f), 1, 2 * (f.hash && f.zero && n));
	ret += ft_putnstr_fd(ft_flag(f), 1, 2 * (f.hash && !f.zero && n));
	ret += ft_putnchar_fd('0', 1, f.precision - len);
	if (len)
		ret += ft_hex(f, n, n);
	return (ret);
}
