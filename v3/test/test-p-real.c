/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test-p-real.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/16 00:42:01 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/16 11:55:30 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int	main(void)
{
	char	str[] = "Hello, world!";
	int		num;
	int		tmp;
	float	decimal;
	void	*ptr = NULL;

	num = 42;
	decimal = 3.14;
	tmp = printf("String: %p\n", str);
	printf("Return value for printf: %d\n", tmp);
	tmp = printf("Integer: %p\n", &num);
	printf("Return value for printf: %d\n", tmp);
	tmp = printf("Float: %p\n", &decimal);
	printf("Return value for printf: %d\n", tmp);
	tmp = printf("Null pointer: %p\n", ptr);
	printf("Return value for printf: %d\n", tmp);
	tmp = printf("Hexadecimal address: %p\n", &tmp);
	printf("Return value for printf: %d\n", tmp);
	tmp = printf("Pointer to str: %p\n", str);
	printf("Return value for printf: %d\n", tmp);
	tmp = printf("Pointer to num: %p\n", &num);
	printf("Return value for printf: %d\n", tmp);
	tmp = printf("Pointer to decimal: %p\n", &decimal);
	printf("Return value for printf: %d\n", tmp);
	tmp = printf("Pointer to ptr: %p\n", &ptr);
	printf("Return value for printf: %d\n", tmp);
	tmp = printf("Pointer to printf: %p\n", &printf);
	printf("Return value for printf: %d\n", tmp);
	return (0);
}
