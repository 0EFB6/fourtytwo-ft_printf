/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/11 13:27:17 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/19 09:56:41 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <limits.h>
#include "ft_printf.h"

int	main(void)
{
	int	tmp;
	//int	num = 10;

	//ft_printf("Number: %+i\n", 42);
	//ft_printf("Float: %u\n", 429496739);
	//ft_printf("%+d\n", 2147483647);
	//write(1, "\n\n", 2);
	//ft_printf("h3e %x\n", num);
	//ft_printf("%#x\n", num);
	//ft_printf("%X\n", num);
	//ft_printf("%#X\n", num);
	//char	word[] = "hello world! nice to meet you!";
	//ft_printf("Test%.9s\n", word);
	//ft_printf("Test% d\n", num);
	//int tmp = printf("Test%010u\n", num);
	//int tmp = ft_printf("HAHA%dLOL\n", num);
	//printf("TMP%d\n", tmp);

	//char string[] = "Hello, World!";
	//ft_printf("String with precision: %-s\n", 26, string);
	//ft_printf(" %.2d\n", -1);
	//printf(" %.2d\n", -1);
	tmp = ft_printf("%-1c %-2c %-3c\n", '0', 0, '1');
	printf("TMP%d\n", tmp);
	tmp = printf("%-1c %-2c %-3c\n", '0', 0, '1');
	printf("TMP%d\n", tmp);

	printf("SPACE============\n");

	tmp = ft_printf(" % dLOL\n", INT_MIN);
	printf("TMP%d\n", tmp);
	tmp = printf(" % dLOL\n", INT_MIN);
	printf("TMP%d\n", tmp);
	tmp = ft_printf(" % iLOL\n", INT_MIN);
	printf("TMP%d\n", tmp);
	tmp = printf(" % iLOL\n", INT_MIN);
	printf("TMP%d\n", tmp);

	printf("PLUS===========\n");

	tmp = ft_printf(" %+dLOL\n", INT_MIN);
	printf("TMP%d\n", tmp);
	tmp = printf(" %+dLOL\n", INT_MIN);
	printf("TMP%d\n", tmp);
	tmp = ft_printf(" %+iLOL\n", INT_MIN);
	printf("TMP%d\n", tmp);
	tmp = printf(" %+iLOL\n", INT_MIN);
	printf("TMP%d\n", tmp);

	printf("PLUS===========\n");
	tmp = ft_printf(" %u\n", -3);
	printf("TMP%d\n", tmp);
	tmp = printf(" %u\n", -3);
	printf("TMP%d\n", tmp);
	return (0);
}
