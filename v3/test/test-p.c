/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test-p.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/16 00:40:41 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/16 11:55:53 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_printf.h"

int	main(void)
{
	char	str[] = "Hello, world!";
	int		num;
	int		tmp;
	float	decimal;
	void	*ptr = NULL;

	num = 42;
	decimal = 3.14;
	tmp = ft_printf("String: %p\n", str);
	printf("Return value for ft_printf: %d\n", tmp);
	tmp = ft_printf("Integer: %p\n", &num);
	printf("Return value for ft_printf: %d\n", tmp);
	tmp = ft_printf("Float: %p\n", &decimal);
	printf("Return value for ft_printf: %d\n", tmp);
	tmp = ft_printf("Null pointer: %p\n", ptr);
	printf("Return value for ft_printf: %d\n", tmp);
	tmp = ft_printf("Hexadecimal address: %p\n", &tmp);
	printf("Return value for ft_printf: %d\n", tmp);
	tmp = ft_printf("Pointer to str: %p\n", str);
	printf("Return value for ft_printf: %d\n", tmp);
	tmp = ft_printf("Pointer to num: %p\n", &num);
	printf("Return value for ft_printf: %d\n", tmp);
	tmp = ft_printf("Pointer to decimal: %p\n", &decimal);
	printf("Return value for ft_printf: %d\n", tmp);
	tmp = ft_printf("Pointer to ptr: %p\n", &ptr);
	printf("Return value for ft_printf: %d\n", tmp);
	tmp = ft_printf("Pointer to ft_printf: %p\n", &ft_printf);
	printf("Return value for ft_printf: %d\n", tmp);
	return (0);
}
