/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test-percent.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/16 00:48:05 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/16 11:57:00 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_printf.h"

int	main(void)
{
	int		tmp1;
	int		tmp2;
	int		tmp3;
	int		tmp4;
	int		tmp5;
	int		tmp6;
	int		tmp7;
	int		tmp8;
	int		tmp9;
	int		tmp10;
	char	str[] = "hello world!";

	tmp1 = ft_printf("%%\n");
	printf("Return value for ft_printf: %d\n", tmp1);
	tmp2 = ft_printf("Testing multiple %% signs: %% %% %%\n");
	printf("Return value for ft_printf: %d\n", tmp2);
	tmp3 = ft_printf("String with %%: %s\n", str);
	printf("Return value for ft_printf: %d\n", tmp3);
	tmp4 = ft_printf("Decimal number with %%: %%d\n", 42);
	printf("Return value for ft_printf: %d\n", tmp4);
	tmp5 = ft_printf("Hexadecimal number with %%: %%x\n", 255);
	printf("Return value for ft_printf: %d\n", tmp5);
	tmp6 = ft_printf("Floating point number with %%: %%f\n", 3.14);
	printf("Return value for ft_printf: %d\n", tmp6);
	tmp7 = ft_printf("Character with %%: %%c\n", 'A');
	printf("Return value for ft_printf: %d\n", tmp7);
	tmp8 = ft_printf("Unsigned decimal number with %%: %%u\n", 12345);
	printf("Return value for ft_printf: %d\n", tmp8);
	tmp9 = ft_printf("Octal number with %%: %%o\n", 63);
	printf("Return value for ft_printf: %d\n", tmp9);
	tmp10 = ft_printf("Long long number with %%: %%lld\n", (long long)9876543210);
	printf("Return value for ft_printf: %d\n", tmp10);
	return (0);
}
