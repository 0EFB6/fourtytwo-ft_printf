/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test-X-real.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/16 00:46:13 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/16 13:50:58 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int	main(void)
{
	int	ret;

	ret = printf("Test case 1: %X\n", 42);
	printf("Ret value for printf: %d\n", ret);
	ret = printf("Test case 2: %X\n", -42);
	printf("Ret value for printf: %d\n", ret);
	ret = printf("Test case 3: %X\n", 0);
	printf("Ret value for printf: %d\n", ret);
	ret = printf("Test case 4: %X\n", 123456789);
	printf("Ret value for printf: %d\n", ret);
	ret = printf("Test case 5: %X\n", -123456789);
	printf("Ret value for printf: %d\n", ret);
	ret = printf("Test case 6: %X\n", 0xABCDEF);
	printf("Ret value for printf: %d\n", ret);
	ret = printf("Test case 7: %X\n", 0xFFFFFFFF);
	printf("Ret value for printf: %d\n", ret);
	ret = printf("Test case 8: %X\n", -0xFFFFFFFF);
	printf("Ret value for printf: %d\n", ret);
	ret = printf("Test case 9: %X\n", 0x7FFFFFFF);
	printf("Ret value for printf: %d\n", ret);
	ret = printf("Test case 10: %X\n", -0x7FFFFFFF);
	printf("Ret value for printf: %d\n", ret);
	return (0);
}
