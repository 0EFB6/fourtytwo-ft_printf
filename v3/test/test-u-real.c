/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test-u-real.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/15 23:40:55 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/16 12:01:07 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int	main(void)
{
	unsigned int	num1;
	unsigned int	num2;
	unsigned int	num3;
	unsigned int	num4;
	unsigned int	num5;
	unsigned int	num6;
	unsigned int	num7;
	unsigned int	num8;
	unsigned int	num9;
	unsigned int	num10;
	int				tmp;

	num1 = 0;
	num2 = 12345;
	num3 = 987654321;
	num4 = 4294967295;
	num5 = 999999;
	num6 = 1;
	num7 = 777;
	num8 = 8888;
	num9 = 1000000;
	num10 = 123;
	tmp = printf("Number: %u\n", num1);
	printf("Ret value for printf: %d\n\n", tmp);
	tmp = printf("Number: %u\n", num2);
	printf("Ret value for printf: %d\n\n", tmp);
	tmp = printf("Number: %u\n", num3);
	printf("Ret value for printf: %d\n\n", tmp);
	tmp = printf("Number: %u\n", num4);
	printf("Ret value for printf: %d\n\n", tmp);
	tmp = printf("Number: %u\n", num5);
	printf("Ret value for printf: %d\n\n", tmp);
	tmp = printf("Number: %u\n", num6);
	printf("Ret value for printf: %d\n\n", tmp);
	tmp = printf("Number: %u\n", num7);
	printf("Ret value for printf: %d\n\n", tmp);
	tmp = printf("Number: %u\n", num8);
	printf("Ret value for printf: %d\n\n", tmp);
	tmp = printf("Number: %u\n", num9);
	printf("Ret value for printf: %d\n\n", tmp);
	tmp = printf("Number: %u\n", num10);
	printf("Ret value for printf: %d\n\n", tmp);
	return (0);
}
