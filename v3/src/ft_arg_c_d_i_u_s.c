/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arg_d_i_u.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/15 16:30:08 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/18 12:54:55 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_arg_c(t_format f, va_list arg)
{
	char	c;
	int		ret;

	ret = 0;
	if (f.specifier == 'c')
		c = va_arg(arg, int);
	else
		c = '%';
	ret += ft_putnchar_fd(c, 1, 1);
	return (ret);
}

int	ft_arg_d_i_u(t_format f, va_list arg)
{
	char	*nbr;
	int		n;
	int		ret;
	int		len;
	int		negative;

	n = va_arg(arg, int);
	ret = 0;
	negative = (n < 0 && n != INT_MIN && f.specifier != 'u');
	if (n < 0 && f.specifier != 'u')
		n *= -1;
	if (n < 0 && f.specifier == 'u')
		nbr = ft_uitoa((unsigned)n);
	else
		nbr = ft_itoa(n);
	len = ft_strlen(nbr);
	if (negative)
		ret += ft_putnchar_fd('-', 1, 1);
	ret += write(1, nbr, len);
	free(nbr);
	return (ret);
}

int	ft_arg_s(t_format f, va_list arg)
{
	char	*str;

	str = va_arg(arg, char *);
	if (!str)
		str = "(null)";
	f.precision = ft_strlen(str);
	return (ft_putnstr_fd(str, 1, f.precision));
}
