# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: cwei-she <cwei-she@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2023/06/09 23:17:22 by cwei-she          #+#    #+#              #
#    Updated: 2023/06/19 09:01:21 by cwei-she         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libftprintf.a

FILES = ft_arg_c_d_i_u_s ft_arg_p_x ft_parse ft_printf ft_utils
FILES_B = ft_arg_c_s_bonus ft_arg_d_i_u_bonus ft_arg_p_bonus \
		  ft_arg_x_bonus ft_parse_bonus ft_printf_bonus ft_utils_bonus

LIBFT = ./libft/libft.a

HEADER_PATH = ./include
HEADER_FT_PRINTF = $(HEADER_PATH)/ft_printf.h

CC = cc -Wall -Werror -Wextra -I$(HEADER_PATH)

RM = rm -rf

SRCS_PATH = ./src/
SRCB_PATH = ./srcb/
SRCS = $(addprefix $(SRCS_PATH), $(addsuffix .c, $(FILES)))
SRCB = $(addprefix $(SRCB_PATH), $(addsuffix .c, $(FILES_B)))

OBJS = $(SRCS:.c=.o)
OBJB = $(SRCB:.c=.o)

all:		$(NAME)

.c.o:		
			$(CC) -c $< -o $@

bonus:		$(OBJB) $(LIBFT)
			ar rcs $(NAME) $(OBJB)

$(NAME):	$(OBJS) $(LIBFT)
			ar rcs $(NAME) $(OBJS)

$(LIBFT):
			make bonus -C libft
			cp $(LIBFT) $(NAME)

clean:		
			make clean -C libft
			$(RM) $(OBJS)

fclean:		
			make fclean -C libft
			$(RM) $(OBJS) $(NAME) $(OBJB)

re:			fclean all

norm:
			norminette -R CheckForbiddenSourceHeader
			norminette -R CheckDefine

.PHONY:		all bonus clean fclean re norm
