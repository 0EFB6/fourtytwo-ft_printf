/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/11 13:27:17 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/23 00:52:57 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_printf_bonus.h"

int	main(void)
{
	int	tmp;;

	//printf("######## D #######\n");
	//tmp = ft_printf(" % d\n", INT_MIN);
	//printf("RET:%d\n", tmp);
	//tmp = printf(" % ld\n", INT_MIN);
	//printf("RET:%d\n", tmp);
	//printf("######## I #######\n");
	//tmp = ft_printf(" % i\n", INT_MIN);
	//printf("RET:%d\n", tmp);
	//tmp = printf(" % li\n", INT_MIN);
	//printf("RET:%d\n", tmp);

	printf("######## 001 c width #######\n");
	tmp = ft_printf("%1c", 'a');
    printf("RET:%d\n", tmp);
    tmp = printf("%1c", 'a');
    printf("RET:%d\n", tmp);

	printf("\n######## 002 c width #######\n");
    tmp = ft_printf("%1c", '\0');
    printf("RET:%d\n", tmp);
    tmp = printf("%1c", '\0');
    printf("RET:%d\n", tmp);

	printf("\n######## 003 c width #######\n");
    tmp = ft_printf("%10c", 'b');
    printf("RET:%d\n", tmp);
    tmp = printf("%10c", 'b');
    printf("RET:%d\n", tmp);

	printf("\n######## 004 c width #######\n");
    tmp = ft_printf("%10c", '\0');
    printf("RET:%d\n", tmp);
    tmp = printf("%10c", '\0');
    printf("RET:%d\n", tmp);

	printf("\n######## 005 c width #######\n");
    tmp = ft_printf("%2c", 'c');
    printf("RET:%d\n", tmp);
    tmp = printf("%2c", 'c');
    printf("RET:%d\n", tmp);

	printf("\n######## 006 c width #######\n");
    tmp = ft_printf("there are 15 spaces between this text and the next char%15c", 'd');
    printf("RET:%d\n", tmp);
    tmp = printf("there are 15 spaces between this text and the next char%15c", 'd');
    printf("RET:%d\n", tmp);

	printf("\n######## 007 c width #######\n");
    tmp = ft_printf("%5chis paragraph is indented", 't');
    printf("RET:%d\n", tmp);
    tmp = printf("%5chis paragraph is indented", 't');
    printf("RET:%d\n", tmp);

	printf("\n######## 008 c width #######\n");
    tmp = ft_printf("%5c now you see", '\0');
    printf("RET:%d\n", tmp);
    tmp = printf("%5c now you see", '\0');
    printf("RET:%d\n", tmp);

	printf("\n######## 009 c width #######\n");
    tmp = ft_printf("The number %7c represents luck", '7');
    printf("RET:%d\n", tmp);
    tmp = printf("The number %7c represents luck", '7');
    printf("RET:%d\n", tmp);


	printf("\n//////////////////////////////////////\n");
	printf("///////////// BORDERLINE /////////////\n");
	printf("//////////////////////////////////////\n");


	printf("\n######## 001 s width #######\n");
	tmp = ft_printf("%7s", "a");
	printf("RET:%d\n", tmp);
	tmp = printf("%7s", "a");
	printf("RET:%d\n", tmp);

	printf("\n######## 002 s width #######\n");
	tmp = ft_printf("%7s", "abc");
	printf("RET:%d\n", tmp);
	tmp = printf("%7s", "abc");
	printf("RET:%d\n", tmp);

	printf("\n######## 003 s width #######\n");
	tmp = ft_printf("%4s", "-42");
	printf("RET:%d\n", tmp);
	tmp = printf("%4s", "-42");
	printf("RET:%d\n", tmp);

	printf("\n######## 004 s width #######\n");
	tmp = ft_printf("%5s", "-42");
	printf("RET:%d\n", tmp);
	tmp = printf("%5s", "-42");
	printf("RET:%d\n", tmp);

	printf("\n######## 005 s width #######\n");
	tmp = ft_printf("%6s", "-42");
	printf("RET:%d\n", tmp);
	tmp = printf("%6s", "-42");
	printf("RET:%d\n", tmp);

	printf("\n######## 006 s width #######\n");
	char* null_str = NULL;
	tmp = ft_printf("%7s", null_str);
	printf("RET:%d\n", tmp);
	tmp = printf("%7s", null_str);
	printf("RET:%d\n", tmp);

	printf("\n######## 007 s width #######\n");
	tmp = ft_printf("%7s is as easy as %13s", "abc", "123");
	printf("RET:%d\n", tmp);
	tmp = printf("%7s is as easy as %13s", "abc", "123");
	printf("RET:%d\n", tmp);

	printf("\n######## 008 s width #######\n");
	tmp = ft_printf("%13s are the three first letter of the %3s", "a, b and c", "alphabet");
	printf("RET:%d\n", tmp);
	tmp = printf("%13s are the three first letter of the %3s", "a, b and c", "alphabet");
	printf("RET:%d\n", tmp);

	printf("\n######## 009 s width #######\n");
	tmp = ft_printf("%s%13s%42s%3s", "a, b and c", " are letters", " of the", " alphabet");
	printf("RET:%d\n", tmp);
	tmp = printf("%s%13s%42s%3s", "a, b and c", " are letters", " of the", " alphabet");
	printf("RET:%d\n", tmp);

	printf("\n######## 010 s width #######\n");
	tmp = ft_printf("%sc%13sd%42sp%3sx", "a, b and c", " are letters", " of the", " alphabet");
	printf("RET:%d\n", tmp);
	tmp = printf("%sc%13sd%42sp%3sx", "a, b and c", " are letters", " of the", " alphabet");
	printf("RET:%d\n", tmp);

	printf("\n######## 011 s width #######\n");
	tmp = ft_printf("%sc%13sd%42sp%3sx", "a, b and c", " are letters", " of the", " alphabet");
	printf("RET:%d\n", tmp);
	tmp = printf("%sc%13sd%42sp%3sx", "a, b and c", " are letters", " of the", " alphabet");
	printf("RET:%d\n", tmp);

	printf("\n//////////////////////////////////////\n");
	printf("///////////// BORDERLINE /////////////\n");
	printf("//////////////////////////////////////\n");

	printf("\n######## 001 p width #######\n");
	char	c = 'a';
	tmp = ft_printf("%30p", &c);
	printf("RET:%d\n", tmp);
	tmp = printf("%30p", &c);
	printf("RET:%d\n", tmp);
	
	printf("\n######## 002 p width #######\n");
	tmp = ft_printf("%30p", &c);
	printf("RET:%d\n", tmp);
	tmp = printf("%30p", &c);
	printf("RET:%d\n", tmp);
	
	printf("\n######## 003 p width #######\n");
	tmp = ft_printf("%8p", (void *)0);
	printf("RET:%d\n", tmp);
	tmp = printf("%8p", (void *)0);
	printf("RET:%d\n", tmp);
	
	printf("\n######## 004 p width #######\n");
	tmp = ft_printf("%8p is the address", (void *)0);
	printf("RET:%d\n", tmp);
	tmp = printf("%8p is the address", (void *)0);
	printf("RET:%d\n", tmp);


	printf("\n//////////////////////////////////////\n");
	printf("///////////// BORDERLINE /////////////\n");
	printf("//////////////////////////////////////\n");


	printf("\n######## 001 d width #######\n");
	tmp = ft_printf("%10d", 42);
	printf("RET:%d\n", tmp);
	tmp = printf("%10d", 42);
	printf("RET:%d\n", tmp);
	
	printf("\n######## 002 d width #######\n");
	tmp = ft_printf("%42d", 42000);
	printf("RET:%d\n", tmp);
	tmp = printf("%42d", 42000);
	printf("RET:%d\n", tmp);
	
	printf("\n######## 003 d width #######\n");
	tmp = ft_printf("%20d", -42000);
	printf("RET:%d\n", tmp);
	tmp = printf("%20d", -42000);
	printf("RET:%d\n", tmp);
	
	printf("\n######## 004 d width #######\n");
	tmp = ft_printf("wait for it... %50d", 42);
	printf("RET:%d\n", tmp);
	tmp = printf("wait for it... %50d", 42);
	printf("RET:%d\n", tmp);
	
	printf("\n######## 005 d width #######\n");
	tmp = ft_printf("%20d is how many tests are going to be made", 8000);
	printf("RET:%d\n", tmp);
	tmp = printf("%20d is how many tests are going to be made", 8000);
	printf("RET:%d\n", tmp);
	
	printf("\n######## 006 d width #######\n");
	tmp = ft_printf("%30d", 2147483647);
	printf("RET:%d\n", tmp);
	tmp = printf("%30d", 2147483647);
	printf("RET:%d\n", tmp);
	
	printf("\n######## 007 d width #######\n");
	tmp = ft_printf("%30d", (int)-2147483648);
	printf("RET:%d\n", tmp);
	tmp = printf("%30d", (int)-2147483648);
	printf("RET:%d\n", tmp);
	
	printf("\n######## 008 d width #######\n");
	tmp = ft_printf("%12d", (int)-2147483648);
	printf("RET:%d\n", tmp);
	tmp = printf("%12d", (int)-2147483648);
	printf("RET:%d\n", tmp);
	
	printf("\n######## 009 d width #######\n");
	tmp = ft_printf("%12d, %20d, %2d, %42d", (int)-2147483648, 3, 30, -1);
	printf("RET:%d\n", tmp);
	tmp = printf("%12d, %20d, %2d, %42d", (int)-2147483648, 3, 30, -1);
	printf("RET:%d\n", tmp);
	
	printf("\n######## 010 d width #######\n");
	tmp = ft_printf("%12d, %d, %2d, %42d", (int)-2147483648, 3, 30, -1);
	printf("RET:%d\n", tmp);
	tmp = printf("%12d, %d, %2d, %42d", (int)-2147483648, 3, 30, -1);
	printf("RET:%d\n", tmp);
	
	printf("\n######## 011 d width #######\n");
	tmp = ft_printf("%14d%20d%2d%d", (int)-2147483648, 3, 30, -1);
	printf("RET:%d\n", tmp);
	tmp = printf("%14d%20d%2d%d", (int)-2147483648, 3, 30, -1);
	printf("RET:%d\n", tmp);
	
	printf("\n######## 012 d width #######\n");
	tmp = ft_printf("%14dc%20ds%2dx%du", (int)-2147483648, 3, 30, -1);
	printf("RET:%d\n", tmp);
	tmp = printf("%14dc%20ds%2dx%du", (int)-2147483648, 3, 30, -1);
	printf("RET:%d\n", tmp);

	printf("\n//////////////////////////////////////\n");
	printf("///////////// BORDERLINE /////////////\n");
	printf("//////////////////////////////////////\n");

	printf("\n######## 001 i width #######\n");
	tmp = ft_printf("%10i", 42);
	printf("RET:%d\n", tmp);
	tmp = printf("%10i", 42);
	printf("RET:%d\n", tmp);
	
	printf("\n######## 002 i width #######\n");
	tmp = ft_printf("%10i", 42);
	printf("RET:%d\n", tmp);
	tmp = printf("%10i", 42);
	printf("RET:%d\n", tmp);

	printf("\n######## 003 i width #######\n");
	tmp = ft_printf("%42i", 42000);
	printf("RET:%d\n", tmp);
	tmp = printf("%42i", 42000);
	printf("RET:%d\n", tmp);

	printf("\n######## 004 i width #######\n");
	tmp = ft_printf("%20i", -42000);
	printf("RET:%d\n", tmp);
	tmp = printf("%20i", -42000);
	printf("RET:%d\n", tmp);

	printf("\n######## 005 i width #######\n");
	tmp = ft_printf("wait for it... %50i", 42);
	printf("RET:%d\n", tmp);
	tmp = printf("wait for it... %50i", 42);
	printf("RET:%d\n", tmp);

	printf("\n######## 006 i width #######\n");
	tmp = ft_printf("%20i is how many tests are going to be made", 8000);
	printf("RET:%d\n", tmp);
	tmp = printf("%20i is how many tests are going to be made", 8000);
	printf("RET:%d\n", tmp);

	printf("\n######## 007 i width #######\n");
	tmp = ft_printf("%30i", 2147483647);
	printf("RET:%d\n", tmp);
	tmp = printf("%30i", 2147483647);
	printf("RET:%d\n", tmp);

	printf("\n######## 008 i width #######\n");
	tmp = ft_printf("%30i", (int)-2147483648);
	printf("RET:%d\n", tmp);
	tmp = printf("%30i", (int)-2147483648);
	printf("RET:%d\n", tmp);

	printf("\n######## 009 i width #######\n");
	tmp = ft_printf("%12i", (int)-2147483648);
	printf("RET:%d\n", tmp);
	tmp = printf("%12i", (int)-2147483648);
	printf("RET:%d\n", tmp);

	printf("\n######## 010 i width #######\n");
	tmp = ft_printf("%12i, %20i, %2i, %42i", (int)-2147483648, 3, 30, -1);
	printf("RET:%d\n", tmp);
	tmp = printf("%12i, %20i, %2i, %42i", (int)-2147483648, 3, 30, -1);
	printf("RET:%d\n", tmp);
	
	printf("\n######## 011 i width #######\n");
	tmp = ft_printf("%12i, %i, %2i, %42i", (int)-2147483648, 3, 30, -1);
	printf("RET:%d\n", tmp);
	tmp = printf("%12i, %i, %2i, %42i", (int)-2147483648, 3, 30, -1);
	printf("RET:%d\n", tmp);
	
	printf("\n######## 012 i width #######\n");
	tmp = ft_printf("%14i%20i%2i%i", (int)-2147483648, 3, 30, -1);
	printf("RET:%d\n", tmp);
	tmp = printf("%14i%20i%2i%i", (int)-2147483648, 3, 30, -1);
	printf("RET:%d\n", tmp);
	
	printf("\n######## 013 i width #######\n");
	tmp = ft_printf("%14ic%20is%2ix%du", (int)-2147483648, 3, 30, -1);
	printf("RET:%d\n", tmp);
	tmp = printf("%14ic%20is%2ix%du", (int)-2147483648, 3, 30, -1);
	printf("RET:%d\n", tmp);
	

	printf("\n//////////////////////////////////////\n");
	printf("///////////// BORDERLINE /////////////\n");
	printf("//////////////////////////////////////\n");


	printf("\n######## 001 u width #######\n");
    tmp = ft_printf("%2u", 1);
    printf("RET:%d\n", tmp);
    tmp = printf("%2u", 1);
    printf("RET:%d\n", tmp);
    
    printf("\n######## 002 u width #######\n");
    tmp = ft_printf("%30u", 1000);
    printf("RET:%d\n", tmp);
    tmp = printf("%30u", 1000);
    printf("RET:%d\n", tmp);
    
    printf("\n######## 003 u width #######\n");
    tmp = ft_printf("%9u is the biggest unsigned int", (unsigned int)-1);
    printf("RET:%d\n", tmp);
    tmp = printf("%9u is the biggest unsigned int", (unsigned int)-1);
    printf("RET:%d\n", tmp);
    
    printf("\n######## 004 u width #######\n");
    tmp = ft_printf("%10uis the biggest unsigned int", (unsigned int)-1);
    printf("RET:%d\n", tmp);
    tmp = printf("%10uis the biggest unsigned int", (unsigned int)-1);
    printf("RET:%d\n", tmp);
    
    printf("\n######## 005 u width #######\n");
    tmp = ft_printf("%11uis the biggest unsigned int", (unsigned int)-1);
    printf("RET:%d\n", tmp);
    tmp = printf("%11uis the biggest unsigned int", (unsigned int)-1);
    printf("RET:%d\n", tmp);
    
    printf("\n######## 006 u width #######\n");
    tmp = ft_printf("the biggest unsigned int is %9u", (unsigned int)-1);
    printf("RET:%d\n", tmp);
    tmp = printf("the biggest unsigned int is %9u", (unsigned int)-1);
    printf("RET:%d\n", tmp);
    
    printf("\n######## 007 u width #######\n");
    tmp = ft_printf("the biggest unsigned int is %10u", (unsigned int)-1);
    printf("RET:%d\n", tmp);
    tmp = printf("the biggest unsigned int is %10u", (unsigned int)-1);
    printf("RET:%d\n", tmp);
    
    printf("\n######## 008 u width #######\n");
    tmp = ft_printf("the biggest unsigned int is %11u", (unsigned int)-1);
    printf("RET:%d\n", tmp);
    tmp = printf("the biggest unsigned int is %11u", (unsigned int)-1);
    printf("RET:%d\n", tmp);
    
    printf("\n######## 009 u width #######\n");
    tmp = ft_printf("Here are some numbers: %1u%2u%5u%3u%9u and %ui", 11, (unsigned int)-1, 2, 200, 3, 10);
    printf("RET:%d\n", tmp);
    tmp = printf("Here are some numbers: %1u%2u%5u%3u%9u and %ui", 11, (unsigned int)-1, 2, 200, 3, 10);
    printf("RET:%d\n", tmp);

	printf("\n//////////////////////////////////////\n");
	printf("///////////// BORDERLINE /////////////\n");
	printf("//////////////////////////////////////\n");

	printf("\n######## 001 x width #######\n");
	tmp = ft_printf("%2x", 1);
	printf("RET:%d\n", tmp);
	tmp = printf("%2x", 1);
	printf("RET:%d\n", tmp);

	printf("\n######## 002 x width #######\n");
	tmp = ft_printf("%3x", 10);
	printf("RET:%d\n", tmp);
	tmp = printf("%3x", 10);
	printf("RET:%d\n", tmp);

	printf("\n######## 003 x width #######\n");
	tmp = ft_printf("%3x", 255);
	printf("RET:%d\n", tmp);
	tmp = printf("%3x", 255);
	printf("RET:%d\n", tmp);

	printf("\n######## 004 x width #######\n");
	tmp = ft_printf("%42x", 256);
	printf("RET:%d\n", tmp);
	tmp = printf("%42x", 256);
	printf("RET:%d\n", tmp);

	printf("\n######## 005 x width #######\n");
	tmp = ft_printf("%9x", (unsigned int)3735929054);
	printf("RET:%d\n", tmp);
	tmp = printf("%9x", (unsigned int)3735929054);
	printf("RET:%d\n", tmp);

	printf("\n######## 006 x width #######\n");
	tmp = ft_printf("the password is %9x", (unsigned int)3735929054);
	printf("RET:%d\n", tmp);
	tmp = printf("the password is %9x", (unsigned int)3735929054);
	printf("RET:%d\n", tmp);

	printf("\n######## 007 x width #######\n");
	tmp = ft_printf("%3x is the definitive answer", (unsigned int)66);
	printf("RET:%d\n", tmp);
	tmp = printf("%3x is the definitive answer", (unsigned int)66);
	printf("RET:%d\n", tmp);

	printf("\n######## 008 x width #######\n");
	tmp = ft_printf("this is the real number: %9x", (unsigned int)-1);
	printf("RET:%d\n", tmp);
	tmp = printf("this is the real number: %9x", (unsigned int)-1);
	printf("RET:%d\n", tmp);

	printf("\n######## 009 x width #######\n");
	tmp = ft_printf("%1x%2x%9x", (unsigned int)-1, 0xf0ca, 123456);
	printf("RET:%d\n", tmp);
	tmp = printf("%1x%2x%9x", (unsigned int)-1, 0xf0ca, 123456);
	printf("RET:%d\n", tmp);

	printf("\n######## 010 x width #######\n");
	tmp = ft_printf("%1xis doomed%2xpost%9xX args", (unsigned int)-1, 0xf0b1a, 7654321);
	printf("RET:%d\n", tmp);
	tmp = printf("%1xis doomed%2xpost%9xX args", (unsigned int)-1, 0xf0b1a, 7654321);
	printf("RET:%d\n", tmp);

	printf("\n//////////////////////////////////////\n");
	printf("///////////// BORDERLINE /////////////\n");
	printf("//////////////////////////////////////\n");

	printf("\n######## 001 X width #######\n");
    tmp = ft_printf("%2X", 1);
    printf("RET:%d\n", tmp);
    tmp = printf("%2X", 1);
    printf("RET:%d\n", tmp);
    
	printf("\n######## 002 X width #######\n");
    tmp = ft_printf("%3X", 10);
    printf("RET:%d\n", tmp);
    tmp = printf("%3X", 10);
    printf("RET:%d\n", tmp);
    
	printf("\n######## 003 X width #######\n");
    tmp = ft_printf("%2X", 160);
    printf("RET:%d\n", tmp);
    tmp = printf("%2X", 160);
    printf("RET:%d\n", tmp);
    
	printf("\n######## 004 X width #######\n");
    tmp = ft_printf("%3X", 255);
    printf("RET:%d\n", tmp);
    tmp = printf("%3X", 255);
    printf("RET:%d\n", tmp);
    
	printf("\n######## 005 X width #######\n");
    tmp = ft_printf("%42X", 256);
    printf("RET:%d\n", tmp);
    tmp = printf("%42X", 256);
    printf("RET:%d\n", tmp);
    
	printf("\n######## 006 X width #######\n");
    tmp = ft_printf("%7X", (unsigned int)3735929054);
    printf("RET:%d\n", tmp);
    tmp = printf("%7X", (unsigned int)3735929054);
    printf("RET:%d\n", tmp);
    
	printf("\n######## 007 X width #######\n");
    tmp = ft_printf("%8X", (unsigned int)3735929054);
    printf("RET:%d\n", tmp);
    tmp = printf("%8X", (unsigned int)3735929054);
    printf("RET:%d\n", tmp);
    
	printf("\n######## 008 X width #######\n");
    tmp = ft_printf("%9X", (unsigned int)3735929054);
    printf("RET:%d\n", tmp);
    tmp = printf("%9X", (unsigned int)3735929054);
    printf("RET:%d\n", tmp);
    
	printf("\n######## 009 X width #######\n");
    tmp = ft_printf("the password is %7X", (unsigned int)3735929054);
    printf("RET:%d\n", tmp);
    tmp = printf("the password is %7X", (unsigned int)3735929054);
    printf("RET:%d\n", tmp);
    
	printf("\n######## 010 X width #######\n");
    tmp = ft_printf("the password is %8X", (unsigned int)3735929054);
    printf("RET:%d\n", tmp);
    tmp = printf("the password is %8X", (unsigned int)3735929054);
    printf("RET:%d\n", tmp);
    
	printf("\n######## 011 X width #######\n");
    tmp = ft_printf("the password is %9X", (unsigned int)3735929054);
    printf("RET:%d\n", tmp);
    tmp = printf("the password is %9X", (unsigned int)3735929054);
    printf("RET:%d\n", tmp);
    
	printf("\n######## 012 X width #######\n");
    tmp = ft_printf("%3X is the definitive answer", (unsigned int)66);
    printf("RET:%d\n", tmp);
    tmp = printf("%3X is the definitive answer", (unsigned int)66);
    printf("RET:%d\n", tmp);
    
	printf("\n######## 013 X width #######\n");
    tmp = ft_printf("this is the real number: %7X", (unsigned int)-1);
    printf("RET:%d\n", tmp);
    tmp = printf("this is the real number: %7X", (unsigned int)-1);
    printf("RET:%d\n", tmp);
    
	printf("\n######## 014 X width #######\n");
    tmp = ft_printf("this is the real number: %8X", (unsigned int)-1);
    printf("RET:%d\n", tmp);
    tmp = printf("this is the real number: %8X", (unsigned int)-1);
    printf("RET:%d\n", tmp);
    
	printf("\n######## 015 X width #######\n");
    tmp = ft_printf("this is the real number: %9X", (unsigned int)-1);
    printf("RET:%d\n", tmp);
    tmp = printf("this is the real number: %9X", (unsigned int)-1);
    printf("RET:%d\n", tmp);
    
	printf("\n######## 016 X width #######\n");
    tmp = ft_printf("%1X%2X%9X", (unsigned int)-1, 0xf0ca, 123456);
    printf("RET:%d\n", tmp);
    tmp = printf("%1X%2X%9X", (unsigned int)-1, 0xf0ca, 123456);
    printf("RET:%d\n", tmp);
    
	printf("\n######## 017 X width #######\n");
    tmp = ft_printf("%1Xis doomed%2Xpost%9Xx args", (unsigned int)-1, 0xf0b1a, 7654321);
    printf("RET:%d\n", tmp);
    tmp = printf("%1Xis doomed%2Xpost%9Xx args", (unsigned int)-1, 0xf0b1a, 7654321);
    printf("RET:%d\n", tmp);

	printf("\n//////////////////////////////////////\n");
	printf("///////////// BORDERLINE /////////////\n");
	printf("//////////////////////////////////////\n");

	printf("\n######## 001 s precision #######\n");
	tmp = ft_printf("%.s", "hi there");
	printf("RET:%d\n", tmp);
	tmp = printf("%.s", "hi there");
	printf("RET:%d\n", tmp);

	printf("\n######## 002 s precision #######\n");
	tmp = ft_printf("%.0s", "hi there");
	printf("RET:%d\n", tmp);
	tmp = printf("%.0s", "hi there");
	printf("RET:%d\n", tmp);

	printf("\n######## 003 s precision #######\n");
	tmp = ft_printf("%.1s", "hi there");
	printf("RET:%d\n", tmp);
	tmp = printf("%.1s", "hi there");
	printf("RET:%d\n", tmp);

	printf("\n######## 004 s precision #######\n");
	tmp = ft_printf("%.2s", "hi there");
	printf("RET:%d\n", tmp);
	tmp = printf("%.2s", "hi there");
	printf("RET:%d\n", tmp);

	printf("\n######## 005 s precision #######\n");
	tmp = ft_printf("%.3s", "hi there");
	printf("RET:%d\n", tmp);
	tmp = printf("%.3s", "hi there");
	printf("RET:%d\n", tmp);

	printf("\n######## 006 s precision #######\n");
	tmp = ft_printf("%.4s", "hi there");
	printf("RET:%d\n", tmp);
	tmp = printf("%.4s", "hi there");
	printf("RET:%d\n", tmp);

	printf("\n######## 007 s precision #######\n");
	tmp = ft_printf("%.7s", "hi there");
	printf("RET:%d\n", tmp);
	tmp = printf("%.7s", "hi there");
	printf("RET:%d\n", tmp);

	printf("\n######## 008 s precision #######\n");
	tmp = ft_printf("%.s", "-42");
	printf("RET:%d\n", tmp);
	tmp = printf("%.s", "-42");
	printf("RET:%d\n", tmp);

	printf("\n######## 009 s precision #######\n");
	tmp = ft_printf("%.0s", "-42");
	printf("RET:%d\n", tmp);
	tmp = printf("%.0s", "-42");
	printf("RET:%d\n", tmp);

	printf("\n######## 010 s precision #######\n");
	tmp = ft_printf("%.1s", "-42");
	printf("RET:%d\n", tmp);
	tmp = printf("%.1s", "-42");
	printf("RET:%d\n", tmp);

	printf("\n######## 011 s precision #######\n");
	tmp = ft_printf("%.2s", "-42");
	printf("RET:%d\n", tmp);
	tmp = printf("%.2s", "-42");
	printf("RET:%d\n", tmp);

	printf("\n######## 012 s precision #######\n");
	tmp = ft_printf("%.1s", null_str);
	printf("RET:%d\n", tmp);
	tmp = printf("%.1s", null_str);
	printf("RET:%d\n", tmp);

	printf("\n######## 013 s precision #######\n");
	tmp = ft_printf("%.2s", null_str);
	printf("RET:%d\n", tmp);
	tmp = printf("%.2s", null_str);
	printf("RET:%d\n", tmp);

	printf("\n######## 014 s precision #######\n");
	tmp = ft_printf("%.5s", null_str);
	printf("RET:%d\n", tmp);
	tmp = printf("%.5s", null_str);
	printf("RET:%d\n", tmp);

	printf("\n######## 015 s precision #######\n");
	tmp = ft_printf("%.2s, motherfucker", "hi there");
	printf("RET:%d\n", tmp);
	tmp = printf("%.2s, motherfucker", "hi there");
	printf("RET:%d\n", tmp);

	printf("\n######## 016 s precision #######\n");
	tmp = ft_printf("This %.3s a triumph ", "wasabi");
	printf("RET:%d\n", tmp);
	tmp = printf("This %.3s a triumph ", "wasabi");
	printf("RET:%d\n", tmp);

	printf("\n######## 017 s precision #######\n");
	tmp = ft_printf("%.4s making a %.4s here: %.13s", "I'm delighted", "notation", "HUGE SUCCESS!");
	printf("RET:%d\n", tmp);
	tmp = printf("%.4s making a %.4s here: %.13s", "I'm delighted", "notation", "HUGE SUCCESS!");
	printf("RET:%d\n", tmp);

	printf("\n######## 018 s precision #######\n");
	tmp = ft_printf("It's %.4s to over%.50s my%s", "hardware", "state", " satisfaction");
	printf("RET:%d\n", tmp);
	tmp = printf("It's %.4s to over%.50s my%s", "hardware", "state", " satisfaction");
	printf("RET:%d\n", tmp);

	printf("\n######## 019 s precision #######\n");
	tmp = ft_printf("%.11s%.6s%.4s", "Aperture", " Scientists", "ce");
	printf("RET:%d\n", tmp);
	tmp = printf("%.11s%.6s%.4s", "Aperture", " Scientists", "ce");
	printf("RET:%d\n", tmp);

	printf("\n######## 020 s precision #######\n");
	tmp = ft_printf("%1.s", "21-school.ru");
	printf("RET:%d\n", tmp);
	tmp = printf("%1.s", "21-school.ru");
	printf("RET:%d\n", tmp);

	printf("\n######## 021 s precision #######\n");
	tmp = ft_printf("%10.1s", "21-school.ru");
	printf("RET:%d\n", tmp);
	tmp = printf("%10.1s", "21-school.ru");
	printf("RET:%d\n", tmp);


	printf("\n//////////////////////////////////////\n");
	printf("///////////// BORDERLINE /////////////\n");
	printf("//////////////////////////////////////\n");


	printf("\n######## 001 d precision ########\n");
	tmp = ft_printf("%.2d", 3);
	printf("RET:%d\n", tmp);
	tmp = printf("%.2d", 3);
	printf("RET:%d\n", tmp);

	printf("\n######## 002 d precision ########\n");
	tmp = ft_printf("%.4d", 32);
	printf("RET:%d\n", tmp);
	tmp = printf("%.4d", 32);
	printf("RET:%d\n", tmp);

	printf("\n######## 003 d precision ########\n");
	tmp = ft_printf("%.3d", -1);
	printf("RET:%d\n", tmp);
	tmp = printf("%.3d", -1);
	printf("RET:%d\n", tmp);

	printf("\n######## 004 d precision ########\n");
	tmp = ft_printf("%.5d", -1234);
	printf("RET:%d\n", tmp);
	tmp = printf("%.5d", -1234);
	printf("RET:%d\n", tmp);

	printf("\n######## 005 d precision ########\n");
	tmp = ft_printf("%.11d", (int)-2147483648);
	printf("RET:%d\n", tmp);
	tmp = printf("%.11d", (int)-2147483648);
	printf("RET:%d\n", tmp);

	printf("\n######## 006 d precision ########\n");
	tmp = ft_printf("%.12d", (int)-2147483648);
	printf("RET:%d\n", tmp);
	tmp = printf("%.12d", (int)-2147483648);
	printf("RET:%d\n", tmp);

	printf("\n######## 007 d precision ########\n");
	tmp = ft_printf("%.13d", (int)-2147483648);
	printf("RET:%d\n", tmp);
	tmp = printf("%.13d", (int)-2147483648);
	printf("RET:%d\n", tmp);

	printf("\n######## 008 d precision ########\n");
	tmp = ft_printf("%.11d", 2147483647);
	printf("RET:%d\n", tmp);
	tmp = printf("%.11d", 2147483647);
	printf("RET:%d\n", tmp);

	printf("\n######## 009 d precision ########\n");
	tmp = ft_printf("%.12d", 2147483647);
	printf("RET:%d\n", tmp);
	tmp = printf("%.12d", 2147483647);
	printf("RET:%d\n", tmp);

	printf("\n######## 010 d precision ########\n");
	tmp = ft_printf("%.0d", 0);
	printf("RET:%d\n", tmp);
	tmp = printf("%.0d", 0);
	printf("RET:%d\n", tmp);

	printf("\n######## 011 d precision ########\n");
	tmp = ft_printf("%.d", 0);
	printf("RET:%d\n", tmp);
	tmp = printf("%.d", 0);
	printf("RET:%d\n", tmp);

	printf("\n######## 012 d precision ########\n");
	tmp = ft_printf("I'm gonna watch %.3d", 7);
	printf("RET:%d\n", tmp);
	tmp = printf("I'm gonna watch %.3d", 7);
	printf("RET:%d\n", tmp);

	printf("\n######## 013 d precision ########\n");
	tmp = ft_printf("%.3d is the movie I'm gonna watch", 7);
	printf("RET:%d\n", tmp);
	tmp = printf("%.3d is the movie I'm gonna watch", 7);
	printf("RET:%d\n", tmp);

	printf("\n######## 014 d precision ########\n");
	tmp = ft_printf("Then take these %.7d things and get the hell out of here", 2);
	printf("RET:%d\n", tmp);
	tmp = printf("Then take these %.7d things and get the hell out of here", 2);
	printf("RET:%d\n", tmp);

	printf("\n######## 015 d precision ########\n");
	tmp = ft_printf("Bla %.2di bla %.5dsbla bla %.dx bla %.d", 127, 42, 1023, 0);
	printf("RET:%d\n", tmp);
	tmp = printf("Bla %.2di bla %.5dsbla bla %.dx bla %.d", 127, 42, 1023, 0);
	printf("RET:%d\n", tmp);

	printf("\n######## 016 d precision########\n");
	tmp = ft_printf("%.4d%.2d%.20d%.0d%.0d%.d%.d%.d", 127, 0, 1023, 0, (int)-2147483648, 0, 1, (int)-2147483648);
	printf("RET:%d\n", tmp);
	tmp = printf("%.4d%.2d%.20d%.0d%.0d%.d%.d%.d", 127, 0, 1023, 0, (int)-2147483648, 0, 1, (int)-2147483648);
	printf("RET:%d\n", tmp);


	printf("\n//////////////////////////////////////\n");
	printf("///////////// BORDERLINE /////////////\n");
	printf("//////////////////////////////////////\n");


	printf("\n######## 001 i precision #######\n");
    tmp = ft_printf("%.3i", 7);
    printf("RET:%d\n", tmp);
    tmp = printf("%.3i", 7);
    printf("RET:%d\n", tmp);
    
    printf("\n######## 002 i precision #######\n");
    tmp = ft_printf("%.2i", 3);
    printf("RET:%d\n", tmp);
    tmp = printf("%.2i", 3);
    printf("RET:%d\n", tmp);
    
    printf("\n######## 003 i precision #######\n");
    tmp = ft_printf("%.4i", 32);
    printf("RET:%d\n", tmp);
    tmp = printf("%.4i", 32);
    printf("RET:%d\n", tmp);
    
    printf("\n######## 004 i precision #######\n");
    tmp = ft_printf("%.3i", -1);
    printf("RET:%d\n", tmp);
    tmp = printf("%.3i", -1);
    printf("RET:%d\n", tmp);
    
    printf("\n######## 005 i precision #######\n");
    tmp = ft_printf("%.5i", -1234);
    printf("RET:%d\n", tmp);
    tmp = printf("%.5i", -1234);
    printf("RET:%d\n", tmp);
    
    printf("\n######## 006 i precision #######\n");
    tmp = ft_printf("%.11i", (int)-2147483648);
    printf("RET:%d\n", tmp);
    tmp = printf("%.11i", (int)-2147483648);
    printf("RET:%d\n", tmp);
    
    printf("\n######## 007 i precision #######\n");
    tmp = ft_printf("%.12i", (int)-2147483648);
    printf("RET:%d\n", tmp);
    tmp = printf("%.12i", (int)-2147483648);
    printf("RET:%d\n", tmp);
    
    printf("\n######## 008 i precision #######\n");
    tmp = ft_printf("%.13i", (int)-2147483648);
    printf("RET:%d\n", tmp);
    tmp = printf("%.13i", (int)-2147483648);
    printf("RET:%d\n", tmp);
    
    printf("\n######## 009 i precision #######\n");
    tmp = ft_printf("%.11i", 2147483647);
    printf("RET:%d\n", tmp);
    tmp = printf("%.11i", 2147483647);
    printf("RET:%d\n", tmp);
    
    printf("\n######## 010 i precision #######\n");
    tmp = ft_printf("%.12i", 2147483647);
    printf("RET:%d\n", tmp);
    tmp = printf("%.12i", 2147483647);
    printf("RET:%d\n", tmp);
    
    printf("\n######## 011 i precision #######\n");
    tmp = ft_printf("%.0i", 0);
    printf("RET:%d\n", tmp);
    tmp = printf("%.0i", 0);
    printf("RET:%d\n", tmp);
    
    printf("\n######## 012 i precision #######\n");
    tmp = ft_printf("%.i", 0);
    printf("RET:%d\n", tmp);
    tmp = printf("%.i", 0);
    printf("RET:%d\n", tmp);
    
    printf("\n######## 013 i precision #######\n");
    tmp = ft_printf("I'm gonna watch %.3i", 7);
    printf("RET:%d\n", tmp);
    tmp = printf("I'm gonna watch %.3i", 7);
    printf("RET:%d\n", tmp);
    
    printf("\n######## 014 i precision #######\n");
    tmp = ft_printf("%.3i is the movie I'm gonna watch", 7);
    printf("RET:%d\n", tmp);
    tmp = printf("%.3i is the movie I'm gonna watch", 7);
    printf("RET:%d\n", tmp);
    
    printf("\n######## 015 i precision #######\n");
    tmp = ft_printf("Then take these %.7i things and get the hell out of here", 2);
    printf("RET:%d\n", tmp);
    tmp = printf("Then take these %.7i things and get the hell out of here", 2);
    printf("RET:%d\n", tmp);
    
    printf("\n######## 016 i precision #######\n");
    tmp = ft_printf("Bla %.2ii bla %.5isbla bla %.ix bla %.i", 127, 42, 1023, 0);
    printf("RET:%d\n", tmp);
    tmp = printf("Bla %.2ii bla %.5isbla bla %.ix bla %.i", 127, 42, 1023, 0);
    printf("RET:%d\n", tmp);
    
    printf("\n######## 017 i precision #######\n");
    tmp = ft_printf("%.4i%.2i%.20i%.0i%.0i%.i%.i%.i", 127, 0, 1023, 0, (int)-2147483648, 0, 1, (int)-2147483648);
    printf("RET:%d\n", tmp);
    tmp = printf("%.4i%.2i%.20i%.0i%.0i%.i%.i%.i", 127, 0, 1023, 0, (int)-2147483648, 0, 1, (int)-2147483648);
    printf("RET:%d\n", tmp);
	
	printf("\n//////////////////////////////////////\n");
	printf("///////////// BORDERLINE /////////////\n");
	printf("//////////////////////////////////////\n");

	printf("\n######## 001 u precision #######\n");
	tmp = ft_printf("%.2u", 1);
	printf("RET:%d\n", tmp);
	tmp = printf("%.2u", 1);
	printf("RET:%d\n", tmp);

	printf("\n######## 002 u precision #######\n");
	tmp = ft_printf("%.2u", 0);
	printf("RET:%d\n", tmp);
	tmp = printf("%.2u", 0);
	printf("RET:%d\n", tmp);

	printf("\n######## 003 u precision #######\n");
	tmp = ft_printf("%.0u", 0);
	printf("RET:%d\n", tmp);
	tmp = printf("%.0u", 0);
	printf("RET:%d\n", tmp);

	printf("\n######## 004 u precision #######\n");
	tmp = ft_printf("%.u", 0);
	printf("RET:%d\n", tmp);
	tmp = printf("%.u", 0);
	printf("RET:%d\n", tmp);

	printf("\n######## 005 u precision #######\n");
	tmp = ft_printf("%.20u", 30000);
	printf("RET:%d\n", tmp);
	tmp = printf("%.20u", 30000);
	printf("RET:%d\n", tmp);

	printf("\n######## 006 u precision #######\n");
	tmp = ft_printf("%.0u", (unsigned int)-1);
	printf("RET:%d\n", tmp);
	tmp = printf("%.0u", (unsigned int)-1);
	printf("RET:%d\n", tmp);

	printf("\n######## 007 u precision #######\n");
	tmp = ft_printf("%.5u", (unsigned int)-1);
	printf("RET:%d\n", tmp);
	tmp = printf("%.5u", (unsigned int)-1);
	printf("RET:%d\n", tmp);

	printf("\n######## 008 u precision #######\n");
	tmp = ft_printf("%.9u", (unsigned int)-1);
	printf("RET:%d\n", tmp);
	tmp = printf("%.9u", (unsigned int)-1);
	printf("RET:%d\n", tmp);

	printf("\n######## 009 u precision #######\n");
	tmp = ft_printf("%.10u", (unsigned int)-1);
	printf("RET:%d\n", tmp);
	tmp = printf("%.10u", (unsigned int)-1);
	printf("RET:%d\n", tmp);

	printf("\n######## 010 u precision #######\n");
	tmp = ft_printf("%.11u", (unsigned int)-1);
	printf("RET:%d\n", tmp);
	tmp = printf("%.11u", (unsigned int)-1);
	printf("RET:%d\n", tmp);

	printf("\n######## 011 u precision #######\n");
	tmp = ft_printf("%.10uis a big number", (unsigned int)-1);
	printf("RET:%d\n", tmp);
	tmp = printf("%.10uis a big number", (unsigned int)-1);
	printf("RET:%d\n", tmp);

	printf("\n######## 012 u precision #######\n");
	tmp = ft_printf("%.0uis a big number", (unsigned int)-1);
	printf("RET:%d\n", tmp);
	tmp = printf("%.0uis a big number", (unsigned int)-1);
	printf("RET:%d\n", tmp);

	printf("\n######## 013 u precision #######\n");
	tmp = ft_printf("%.4us a big number", (unsigned int)-1);
	printf("RET:%d\n", tmp);
	tmp = printf("%.4us a big number", (unsigned int)-1);
	printf("RET:%d\n", tmp);

	printf("\n######## 014 u precision #######\n");
	tmp = ft_printf("%.9uxs a big number", (unsigned int)-1);
	printf("RET:%d\n", tmp);
	tmp = printf("%.9uxs a big number", (unsigned int)-1);
	printf("RET:%d\n", tmp);

	printf("\n######## 015 u precision #######\n");
	tmp = ft_printf("%.11ups a big number", (unsigned int)-1);
	printf("RET:%d\n", tmp);
	tmp = printf("%.11ups a big number", (unsigned int)-1);
	printf("RET:%d\n", tmp);

	printf("\n######## 016 u precision #######\n");
	tmp = ft_printf("the number is %.0u", (unsigned int)-1);
	printf("RET:%d\n", tmp);
	tmp = printf("the number is %.0u", (unsigned int)-1);
	printf("RET:%d\n", tmp);

	printf("\n######## 017 u precision #######\n");
	tmp = ft_printf("the number is %.u", (unsigned int)-1);
	printf("RET:%d\n", tmp);
	tmp = printf("the number is %.u", (unsigned int)-1);
	printf("RET:%d\n", tmp);

	printf("\n######## 018 u precision #######\n");
	tmp = ft_printf("the number is %.5u", (unsigned int)-1);
	printf("RET:%d\n", tmp);
	tmp = printf("the number is %.5u", (unsigned int)-1);
	printf("RET:%d\n", tmp);

	printf("\n######## 019 u precision #######\n");
	tmp = ft_printf("the number is %.9u", (unsigned int)-1);
	printf("RET:%d\n", tmp);
	tmp = printf("the number is %.9u", (unsigned int)-1);
	printf("RET:%d\n", tmp);

	printf("\n######## 020 u precision #######\n");
	tmp = ft_printf("the number is %.10u", (unsigned int)-1);
	printf("RET:%d\n", tmp);
	tmp = printf("the number is %.10u", (unsigned int)-1);
	printf("RET:%d\n", tmp);

	printf("\n######## 021 u precision #######\n");
	tmp = ft_printf("the number is %.11u", (unsigned int)-1);
	printf("RET:%d\n", tmp);
	tmp = printf("the number is %.11u", (unsigned int)-1);
	printf("RET:%d\n", tmp);

	printf("\n######## 022 u precision #######\n");
	tmp = ft_printf("the number is %.11u", (unsigned int)-1);
	printf("RET:%d\n", tmp);
	tmp = printf("the number is %.11u", (unsigned int)-1);
	printf("RET:%d\n", tmp);

	printf("\n######## 023 u precision #######\n");
	tmp = ft_printf("%.0uis a big number", 0);
	printf("RET:%d\n", tmp);
	tmp = printf("%.0uis a big number", 0);
	printf("RET:%d\n", tmp);

	printf("\n######## 024 u precision #######\n");
	tmp = ft_printf("%.4us a big number", 0);
	printf("RET:%d\n", tmp);
	tmp = printf("%.4us a big number", 0);
	printf("RET:%d\n", tmp);

	printf("\n######## 027 u precision #######\n");
	tmp = ft_printf("the number is %.5u", 0);
	printf("RET:%d\n", tmp);
	tmp = printf("the number is %.5u", 0);
	printf("RET:%d\n", tmp);

	printf("\n######## 028 u precision #######\n");
	tmp = ft_printf("%u%.5u%.0u%.u%.9u", 5, 55, 2, 0, 42);
	printf("RET:%d\n", tmp);
	tmp = printf("%u%.5u%.0u%.u%.9u", 5, 55, 2, 0, 42);
	printf("RET:%d\n", tmp);

	printf("\n######## 029 u precision #######\n");
	tmp = ft_printf("%us%.5ui%.0uc%.up%.9ux", 5, 55, 2, 0, 42);
	printf("RET:%d\n", tmp);
	tmp = printf("%us%.5ui%.0uc%.up%.9ux", 5, 55, 2, 0, 42);
	printf("RET:%d\n", tmp);


	printf("\n//////////////////////////////////////\n");
	printf("///////////// BORDERLINE /////////////\n");
	printf("//////////////////////////////////////\n");


	printf("\n######## 001 x precision #######\n");
	tmp = ft_printf("%.4x", 11);
	printf("RET:%d\n", tmp);
	tmp = printf("%.4x", 11);
	printf("RET:%d\n", tmp);

	printf("\n######## 002 x precision #######\n");
	tmp = ft_printf("%.0x", 0);
	printf("RET:%d\n", tmp);
	tmp = printf("%.0x", 0);
	printf("RET:%d\n", tmp);

	printf("\n######## 003 x precision #######\n");
	tmp = ft_printf("%.10x", -1);
	printf("RET:%d\n", tmp);
	tmp = printf("%.10x", -1);
	printf("RET:%d\n", tmp);

	printf("\n######## 004 x precision #######\n");
	tmp = ft_printf("%.14x", -1);
	printf("RET:%d\n", tmp);
	tmp = printf("%.14x", -1);
	printf("RET:%d\n", tmp);

	printf("\n######## 005 x precision #######\n");
	tmp = ft_printf("%.8x", 0);
	printf("RET:%d\n", tmp);
	tmp = printf("%.8x", 0);
	printf("RET:%d\n", tmp);

	printf("\n######## 006 x precision #######\n");
	tmp = ft_printf("%.20x", 30000);
	printf("RET:%d\n", tmp);
	tmp = printf("%.20x", 30000);
	printf("RET:%d\n", tmp);

	printf("\n######## 007 x precision #######\n");
	tmp = ft_printf("%.9x", (unsigned int)-1);
	printf("RET:%d\n", tmp);
	tmp = printf("%.9x", (unsigned int)-1);
	printf("RET:%d\n", tmp);

	printf("\n######## 008 x precision #######\n");
	tmp = ft_printf("%.10x", (unsigned int)-1);
	printf("RET:%d\n", tmp);
	tmp = printf("%.10x", (unsigned int)-1);
	printf("RET:%d\n", tmp);

	printf("\n######## 009 x precision #######\n");
	tmp = ft_printf("%.11x", (unsigned int)-1);
	printf("RET:%d\n", tmp);
	tmp = printf("%.11x", (unsigned int)-1);
	printf("RET:%d\n", tmp);

	printf("\n######## 010 x precision #######\n");
	tmp = ft_printf("%.10xis a big number", (unsigned int)-1);
	printf("RET:%d\n", tmp);
	tmp = printf("%.10xis a big number", (unsigned int)-1);
	printf("RET:%d\n", tmp);

	printf("\n######## 011 x precision #######\n");
	tmp = ft_printf("%.9xxs a big number", (unsigned int)-1);
	printf("RET:%d\n", tmp);
	tmp = printf("%.9xxs a big number", (unsigned int)-1);
	printf("RET:%d\n", tmp);

	printf("\n######## 012 x precision #######\n");
	tmp = ft_printf("the number is %.0x", (unsigned int)-1);
	printf("RET:%d\n", tmp);
	tmp = printf("the number is %.0x", (unsigned int)-1);
	printf("RET:%d\n", tmp);

	printf("\n######## 013 x precision #######\n");
	tmp = ft_printf("the number is %.x", (unsigned int)-1);
	printf("RET:%d\n", tmp);
	tmp = printf("the number is %.x", (unsigned int)-1);
	printf("RET:%d\n", tmp);

	printf("\n######## 014 x precision #######\n");
	tmp = ft_printf("the number is %.5x", (unsigned int)-1);
	printf("RET:%d\n", tmp);
	tmp = printf("the number is %.5x", (unsigned int)-1);
	printf("RET:%d\n", tmp);

	printf("\n######## 015 x precision #######\n");
	tmp = ft_printf("the number is %.9x", (unsigned int)-1);
	printf("RET:%d\n", tmp);
	tmp = printf("the number is %.9x", (unsigned int)-1);
	printf("RET:%d\n", tmp);

	printf("\n######## 016 x precision #######\n");
	tmp = ft_printf("the number is %.10x", (unsigned int)-1);
	printf("RET:%d\n", tmp);
	tmp = printf("the number is %.10x", (unsigned int)-1);
	printf("RET:%d\n", tmp);

	printf("\n######## 017 x precision #######\n");
	tmp = ft_printf("the number is %.11x", (unsigned int)-1);
	printf("RET:%d\n", tmp);
	tmp = printf("the number is %.11x", (unsigned int)-1);
	printf("RET:%d\n", tmp);

	printf("\n######## 018 x precision #######\n");
	tmp = ft_printf("the number is %.11x", (unsigned int)-1);
	printf("RET:%d\n", tmp);
	tmp = printf("the number is %.11x", (unsigned int)-1);
	printf("RET:%d\n", tmp);

	printf("\n######## 019 x precision #######\n");
	tmp = ft_printf("%.0xis a big number", 0);
	printf("RET:%d\n", tmp);
	tmp = printf("%.0xis a big number", 0);
	printf("RET:%d\n", tmp);

	printf("\n######## 020 x precision #######\n");
	tmp = ft_printf("%.4xs a big number", 0);
	printf("RET:%d\n", tmp);
	tmp = printf("%.4xs a big number", 0);
	printf("RET:%d\n", tmp);

	printf("\n######## 021 x precision #######\n");
	tmp = ft_printf("the number is %.5x", 0);
	printf("RET:%d\n", tmp);
	tmp = printf("the number is %.5x", 0);
	printf("RET:%d\n", tmp);

	printf("\n######## 022 x precision #######\n");
	tmp = ft_printf("%x%.5x%.0x%.x%.9x", 5, 55, 2, 0, 42);
	printf("RET:%d\n", tmp);
	tmp = printf("%x%.5x%.0x%.x%.9x", 5, 55, 2, 0, 42);
	printf("RET:%d\n", tmp);

	printf("\n######## 023 x precision #######\n");
	tmp = ft_printf("%xs%.5xi%.0xc%.xp%.9xu", 5, 55, 2, 0, 42);
	printf("RET:%d\n", tmp);
	tmp = printf("%xs%.5xi%.0xc%.xp%.9xu", 5, 55, 2, 0, 42);
	printf("RET:%d\n", tmp);


	printf("\n//////////////////////////////////////\n");
	printf("///////////// BORDERLINE /////////////\n");
	printf("//////////////////////////////////////\n");

	
	return (0);
}
