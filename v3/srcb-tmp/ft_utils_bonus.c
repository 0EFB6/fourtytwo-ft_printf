/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utils_bonus.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/18 13:43:30 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/20 15:05:30 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_bonus.h"

/* Calculate the length of number base on the base given, by default is 10	  */
int	ft_nbrlen_c(long n, int base)
{
	int	count;

	if (n == 0)
		return (1);
	count = 0;
	if (!base)
		base = 10;
	while (n != 0)
	{
		n /= base;
		count++;
	}
	return (count);
}

/* Write out characters to standard output recursively based on the n number */
/* given																	 */
int	ft_putnchar_fd(char c, int fd, int n)
{
	int	i;

	i = 0;
	while (n-- > 0)
		i += (int)write(fd, &c, 1);
	return (i);
}

/* Write out the string to standard output based on the n number given		 */
int	ft_putnstr_fd(char *str, int fd, int n)
{
	if (str != NULL)
		return ((int)write(fd, str, n));
	return (0);
}

/* Converts unsigned int to string											 */
char	*ft_uitoa(unsigned int n)
{
	int				len;
	char			*str;

	len = ft_nbrlen_c(n, 10);
	str = (char *)malloc(len + 1);
	MALLOC_NULL_CHECK(str);
	str[len] = '\0';
	while (len-- > 0)
	{
		str[len] = n % 10 + '0';
		n /= 10;
	}
	return (str);
}

/* Struct: User-defined data type											*/
t_format	ft_format(void)
{
	t_format	format;

	format.minus = 0;
	format.plus = 0;
	format.width = 0;
	format.precision = 0;
	format.negative_precision = 0;
	format.zero = 0;
	format.dot = 0;
	format.space = 0;
	format.hash = 0;
	format.intmin = 0;
	format.specifier = 0;
	return (format);
}
