/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arg_x_bonus.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/15 17:15:26 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/19 15:28:11 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_bonus.h"

/* Returns '0X' or '0x' according to the specifier (either x or X) 			  */
static char	*ft_flag(t_format f)
{
	if (f.specifier == 'X')
		return ("0X");
	return ("0x");
}

/* Iterate again if n is larger than 0 or when iteration is false			  */
/* This is a recusrive function. Prevent leading zero to be added - Iteration */
/* Convert decimals to hexadecimals											  */
static int	ft_hex(t_format f, size_t n, size_t iteration)
{
	int		ret;
	int		remainder;
	char	c;

	ret = 0;
	if (n > 0 || !iteration)
	{
		remainder = n % 16;
		if (f.specifier != 'X')
			c = HEX_LOWER[remainder];
		else
			c = HEX_UP[remainder];
		n /= 16;
		iteration = 1;
		ret += ft_hex(f, n, iteration);
		ret += ft_putnchar_fd(c, 1, 1);
	}
	return (ret);
}

/* Only numbers, '0', '#', '.' and '-' affect %x specifier					 */
/* Calculate length of n using base 16										 */
/* Set len to zero if n is zero while precision is set					 	 */
/* Update precision if the initial precision is less than the actual length	 */
/* of hex																	 */
/* Minus 2 for width if the '#' flag is used.								 */
/* Print '0x' or '0X'if condition is met ('#' and '0')						 */
/* If zero ('0'), print '0' accordingly when '-' flag is not specified		 */
/* Else, print ' 'if not '0' while '-' flag is also not specified			 */
/* Print '0x' or '0X' if condition is met ('#' only)						 */
/* Print '0' if precision is larger than len								 */
/* Call the hex function to print out the hexadecimal if len is valid		 */
/* Print ' ' if '-' flag is specified and width is larger than precision	 */
/* Return the number of written character as return value					 */
int	ft_arg_x(t_format f, va_list arg)
{
	unsigned int	n;
	int				len;
	int				ret;

	n = va_arg(arg, unsigned int);
	len = ft_nbrlen_c(n, 16);
	ret = 0;
	if (!n && f.precision)
		len = 0;
	if (f.precision < len)
		f.precision = len;
	if (f.hash && n)
		f.width -= 2;
	ret += ft_putnstr_fd(ft_flag(f), 1, 2 * (f.hash && f.zero && n));
	if (!f.minus && f.width > f.precision && f.zero)
		ret += ft_putnchar_fd('0', 1, f.width - f.precision);
	else if (!f.minus && f.width > f.precision)
		ret += ft_putnchar_fd(' ', 1, f.width - f.precision);
	ret += ft_putnstr_fd(ft_flag(f), 1, 2 * (f.hash && !f.zero && n));
	ret += ft_putnchar_fd('0', 1, f.precision - len);
	if (len)
		ret += ft_hex(f, n, n);
	if (f.minus && f.width > f.precision)
		ret += ft_putnchar_fd(' ', 1, f.width - f.precision);
	return (ret);
}
