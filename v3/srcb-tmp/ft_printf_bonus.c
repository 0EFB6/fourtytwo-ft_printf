/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_bonus.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/15 11:34:35 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/19 10:00:19 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_bonus.h"

/* The main printf function for my own custom implementation called ft_printf */
/* va_start() initializes the arg_ptr pointer for subsequent calls to		  */
/* va_arg() and va_end().													  */
/* The va_arg() function retrieves a value of the given var_type from the	  */
/* location given by arg_ptr, and increases arg_ptr to point to the next	  */
/* argument in the list. The va_arg() function can retrieve arguments from	  */
/* the list any number of times within the function. The var_type argument	  */
/* must be one of int, long, decimal, double, struct, union, or pointer, or	  */
/* a typedef of one of these types.											  */
/* The va_end() function is needed to indicate the end of parameter scanning. */
/* When encounter '%', execute the parser until it encounters SPECIFIERS	  */
/* and print accordingly to the specifiers									  */
/* If not, write to standard output using ft_putnchar_fd()					  */
/* The while loop will loop till when *str is NULL and va_end the argument	  */
/* Lastly, return the return value which is the number of character printed	  */
/* out																		  */
int	ft_printf(const char *str, ...)
{
	int		ret;
	char	*c;
	va_list	arg;

	ret = 0;
	va_start(arg, str);
	while (*str)
	{
		if (*str == '%')
		{
			c = (char *)str;
			if (*(++str))
				ret += ft_parse((char *)str, arg);
			while (*str && !ft_strchr(SPECIFIERS, *str))
				str++;
			if (!(*str))
				str = c;
		}
		else
			ret += ft_putnchar_fd(*str, 1, 1);
		if (*str)
			str++;
	}
	va_end(arg);
	return (ret);
}
