/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arg_c_s_bonus.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/15 15:35:17 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/22 22:41:44 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_bonus.h"

/* Only numbers, '*' and '-' affect %c specifier						  	  */
/* c is the variable to be printed out										  */
/* Set c to '%' only if specifier is not c									  */
/* Precision is set to 1 because only a character is printed				  */
/* Check if it is numbers first to allow printing actual value after  		  */
/* formatting, print spaces for numbers.			  						  */
/* Then, print the actual character out.									  */
/* If minus is detected, print extra spaces after priting the character		  */
/* (Left justified)															  */
int	ft_arg_c(t_format f, va_list arg)
{
	char	c;
	int		ret;

	ret = 0;
	if (f.specifier == 'c')
		c = va_arg(arg, int);
	else
		c = '%';
	f.precision = 1;
	if (!f.minus && f.width > f.precision)
		ret += ft_putnchar_fd(' ', 1, f.width - f.precision);
	ret += ft_putnchar_fd(c, 1, 1);
	if (f.minus && f.width - f.precision > 0)
		ret += ft_putnchar_fd(' ', 1, f.width - f.precision);
	return (ret);
}

/* Only numbers, '*', '.' and '-' affect %s specifier						  */
/* *str is the variable to be printed out									  */
/* Set str to '(null)' only if str is null									  */
/* Precision is set to strlen of str if '.' do not present or if '.' present  */
/* but its precision is larger than the strlen								  */
/* Check if it is numbers first to allow printing actual value after		  */
/* formatting, print spaces for numbers.			  						  */
/* Then, print the actual character out.									  */
/* If minus is detected, print extra spaces after priting the character		  */
/* (Left justified)															  */
int	ft_arg_s(t_format f, va_list arg)
{
	int		ret;
	char	*str;

	ret = 0;
	str = va_arg(arg, char *);
	if (!str)
	{
		str = "(null)";
		if (f.precision)
			return (0);
	}
	if (!f.dot || f.precision > (int)ft_strlen(str))
		f.precision = ft_strlen(str);
	if (!f.minus && f.width - f.precision > 0)
		ret += ft_putnchar_fd(' ', 1, f.width - f.precision);
	ret += ft_putnstr_fd(str, 1, f.precision);
	if (f.minus && f.width - f.precision > 0)
		ret += ft_putnchar_fd(' ', 1, f.width - f.precision);
	return (ret);
}
