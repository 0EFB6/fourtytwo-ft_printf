/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/15 11:34:35 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/17 23:00:54 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_printf(const char *str, ...)
{
	int		ret;
	char	*c;
	va_list	arg;

	ret = 0;
	va_start(arg, str);
	while (*str)
	{
		if (*str == '%')
		{
			c = (char *)str;
			if (*(++str))
				ret += ft_parse((char *)str, arg);
			while (*str && !ft_strchr(SPECIFIERS, *str))
				str++;
			if (!(*str))
				str = c;
		}
		else
			ret += ft_putnchar_fd(*str, 1, 1);
		if (*str)
			str++;
	}
	va_end(arg);
	return (ret);
}
