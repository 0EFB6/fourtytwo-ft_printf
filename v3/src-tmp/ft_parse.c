/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/15 12:10:57 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/17 23:27:33 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	ft_arg_check(t_format f, va_list arg)
{
	int	ret;

	ret = 0;
	if (f.specifier == 'c' || f.specifier == '%')
		ret = ft_arg_c(f, arg);
	if (f.specifier == 's')
		ret = ft_arg_s(f, arg);
	if (f.specifier == 'd' || f.specifier == 'i' || f.specifier == 'u')
		ret = ft_arg_d_i_u(f, arg);
	if (f.specifier == 'p')
		ret = ft_arg_p(f, arg);
	if (f.specifier == 'x' || f.specifier == 'X')
		ret = ft_arg_x(f, arg);
	return (ret);
}

int	ft_parse(char *str, va_list arg)
{
	t_format	f;

	f = ft_format();
	f.specifier = *str;
	return (ft_arg_check(f, arg));
}
