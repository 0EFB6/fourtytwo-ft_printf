/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arg_p_x.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/15 17:15:43 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/20 17:49:34 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	ft_hex(t_format f, size_t n, size_t iteration)
{
	int		ret;
	int		remainder;
	char	c;

	ret = 0;
	if (n > 0 || (!iteration))
	{
		remainder = n % 16;
		if (f.specifier != 'X')
			c = HEX_LOWER[remainder];
		else
			c = HEX_UP[remainder];
		n /= 16;
		iteration = 1;
		ret += ft_hex(f, n, iteration);
		ret += ft_putnchar_fd(c, 1, 1);
	}
	return (ret);
}

int	ft_arg_p(t_format f, va_list arg)
{
	size_t	n;
	int		ret;

	n = va_arg(arg, size_t);
	ret = 0;
	if (n == 0)
		return (ret + write(1, "0x0", 3));
	ret += write(1, "0x", 2);
	ret += ft_hex(f, n, n);
	return (ret);
}

int	ft_arg_x(t_format f, va_list arg)
{
	unsigned int	n;

	n = va_arg(arg, unsigned int);
	return (ft_hex(f, n, n));
}
