/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/20 17:53:11 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/22 12:32:24 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_printf.h"

int	main(void)
{
	int	tmp;;

	printf("######## c #######\n");
	tmp = ft_printf("%X", (unsigned int)3735929054);
	printf("RET:%d\n", tmp);
	tmp = printf("%X", (unsigned int)3735929054);
	printf("RET:%d\n", tmp);

	return (0);
}