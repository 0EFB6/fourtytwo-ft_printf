/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse_bonus.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/15 12:10:57 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/19 10:28:54 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_bonus.h"

/* To pass the three bonus flag, #,  and +, change the relevant variable */
/* inside struct for further usage. Loops until it encounters SPECIFIERS */
static t_format	ft_parse_bonus(char *str, t_format f)
{
	while (*str != '.' && !ft_strchr(SPECIFIERS, *str))
	{
		if (*str == '+')
			f.plus = 1;
		if (*str == ' ')
			f.space = 1;
		if (*str == '#')
			f.hash = 1;
		str++;
	}
	return (f);
}

/* Update the width variable in struct when parsing the argument		*/
/* The width is the minimum number of characters to be printed out		*/
/* It will print extra spaces if the value is shorter than the width	*/
/* If the flag '-' is specified, the output will be left justified		*/
/* (ie. Spaces on the right)											*/
/*	If there is nothing specified but only N numbers, the output will	*/
/* be right justified (ie. Spaces on the left)							*/
/* Also update the relevant variable for the flag '-' and '0'			*/
/* Loop until it encounter SPECIFIERS and *str is not '.'				*/
/* The extra checking for flag '0' is to ensure that it is after the	*/
/* '%' symbol and not normal number that should be printed				*/
static t_format	ft_parse_width(char *str, va_list arg, t_format f)
{
	int	is_switch_off;

	is_switch_off = 0;
	while (*str != '.' && !ft_strchr(SPECIFIERS, *str))
	{
		if (*str == '-')
			f.minus = 1;
		if (*str == '0' && !ft_isdigit(*(str - 1)))
			f.zero = 1;
		else if (((*str > '0' && *str <= '9') || *str == '*') && !is_switch_off)
		{
			if (*str == '*')
				f.width = va_arg(arg, int);
			else
				f.width = ft_atoi(str);
			is_switch_off = 1;
		}
		str++;
	}
	return (f);
}

/* Update the precision variable in struct when parsing the argument	*/
/* Precision specifies the minimum number of digits to be written		*/
/* If the value to be written is shorter than this number, the result 	*/
/* is padded with leading zeros (for d, i, o, u, x, X)					*/
/* Loop until it encounter SPECIFIERS									*/
static t_format	ft_parse_precision(char *str, va_list arg, t_format f)
{
	int	is_switch_off;

	is_switch_off = 0;
	while (!ft_strchr(SPECIFIERS, *str))
	{
		if ((ft_isdigit(*str) || *str == '*') && !is_switch_off)
		{
			if (*str == '*')
				f.precision = va_arg(arg, int);
			else
				f.precision = ft_atoi(str);
			is_switch_off = 1;
		}
		str++;
	}
	return (f);
}

/* Main parsing function, parse the width first, then bonus, then 		*/
/* precision if the flag '.' is specified.								*/
/* Also update the dot variable if flag '.' is specified				*/
/* After parsing the width precision and special flags, update the		*/
/* specifier variable with the relevant specifier for output			*/
int	ft_parse(char *str, va_list arg)
{
	t_format	format;

	format = ft_parse_width(str, arg, ft_format());
	format = ft_parse_bonus(str, format);
	while (!ft_strchr(SPECIFIERS, *str) && *str != '.')
		str++;
	if (*str == '.' && !format.specifier)
	{
		format.dot = 1;
		format = ft_parse_precision(str++, arg, format);
		while (!ft_strchr(SPECIFIERS, *str))
			str++;
	}
	if (format.width < 0)
	{
		format.minus = 1;
		format.width *= -1;
	}
	format.specifier = *str;
	format.negative_precision = format.precision < 0;
	return (ft_arg_check(format, arg));
}

/* Check the specifier, and prints accordingly to each specifier		*/
int	ft_arg_check(t_format f, va_list arg)
{
	int	ret;

	ret = 0;
	if (f.specifier == 'c' || f.specifier == '%')
		ret = ft_arg_c(f, arg);
	if (f.specifier == 's')
		ret = ft_arg_s(f, arg);
	if (f.specifier == 'd' || f.specifier == 'i' || f.specifier == 'u')
		ret = ft_arg_d_i_u(f, arg);
	if (f.specifier == 'x' || f.specifier == 'X')
		ret = ft_arg_x(f, arg);
	if (f.specifier == 'p')
		ret = ft_arg_p(f, arg);
	return (ret);
}
