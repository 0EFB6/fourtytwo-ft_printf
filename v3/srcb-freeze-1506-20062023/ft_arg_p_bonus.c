/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arg_p_bonus.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/15 17:15:43 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/19 17:20:00 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_bonus.h"

/* Iterate again if n is larger than 0 or when iteration is false			  */
/* This is a recusrive function. Prevent leading zero to be added - Iteration */
/* Convert decimals to hexadecimals											  */
static int	ft_hex(t_format f, size_t n, size_t iteration)
{
	int		ret;
	int		remainder;
	char	c;

	ret = 0;
	if (n > 0 || (!iteration))
	{
		remainder = n % 16;
		if (f.specifier != 'X')
			c = HEX_LOWER[remainder];
		else
			c = HEX_UP[remainder];
		n /= 16;
		iteration = 1;
		ret += ft_hex(f, n, iteration);
		ret += ft_putnchar_fd(c, 1, 1);
	}
	return (ret);
}

/* Only numbers, and '-' affect %p specifier					 			 */
/* Write "(nil)" if n is 0 (null) 											 */
/* Set precision to len if precision is less than len or '.' flag is specify */
/* Write out '0x' and minus 2 for width due to the 2 characters '0x'		 */
/* Call the hex function to print out the pointer value if len is valid		 */
/* Print ' ' if '-' flag is specified and width is larger than precision	 */
/* Return the number of written character as return value					 */
int	ft_arg_p(t_format f, va_list arg)
{
	size_t	n;
	int		ret;
	int		len;

	n = va_arg(arg, size_t);
	ret = 0;
	len = ft_nbrlen_c(n, 16);
	if (n == 0)
		return (write(1, "(nil)", 5));
	if (f.precision < len)
		f.precision = len;
	ret += write(1, "0x", 2);
	f.width -= 2;
	if (len)
		ret += ft_hex(f, n, n);
	if (f.minus && f.width > f.precision)
		ret += ft_putnchar_fd(' ', 1, f.width - f.precision);
	return (ret);
}
