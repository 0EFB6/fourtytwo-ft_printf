/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/11 13:27:17 by cwei-she          #+#    #+#             */
/*   Updated: 2023/06/22 18:08:27 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_printf_bonus.h"

int	main(void)
{
	int	tmp;;

	//printf("######## D #######\n");
	//tmp = ft_printf(" % d\n", INT_MIN);
	//printf("RET:%d\n", tmp);
	//tmp = printf(" % ld\n", INT_MIN);
	//printf("RET:%d\n", tmp);
	//printf("######## I #######\n");
	//tmp = ft_printf(" % i\n", INT_MIN);
	//printf("RET:%d\n", tmp);
	//tmp = printf(" % li\n", INT_MIN);
	//printf("RET:%d\n", tmp);

	printf("######## 001 c width #######\n");
	tmp = ft_printf("%1c", 'a');
    printf("RET:%d\n", tmp);
    tmp = printf("%1c", 'a');
    printf("RET:%d\n", tmp);

	printf("######## 002 c width #######\n");
    tmp = ft_printf("%1c", '\0');
    printf("RET:%d\n", tmp);
    tmp = printf("%1c", '\0');
    printf("RET:%d\n", tmp);

	printf("######## 003 c width #######\n");
    tmp = ft_printf("%10c", 'b');
    printf("RET:%d\n", tmp);
    tmp = printf("%10c", 'b');
    printf("RET:%d\n", tmp);

	printf("######## 004 c width #######\n");
    tmp = ft_printf("%10c", '\0');
    printf("RET:%d\n", tmp);
    tmp = printf("%10c", '\0');
    printf("RET:%d\n", tmp);

	printf("######## 005 c width #######\n");
    tmp = ft_printf("%2c", 'c');
    printf("RET:%d\n", tmp);
    tmp = printf("%2c", 'c');
    printf("RET:%d\n", tmp);

	printf("######## 006 c width #######\n");
    tmp = ft_printf("there are 15 spaces between this text and the next char%15c", 'd');
    printf("RET:%d\n", tmp);
    tmp = printf("there are 15 spaces between this text and the next char%15c", 'd');
    printf("RET:%d\n", tmp);

	printf("######## 007 c width #######\n");
    tmp = ft_printf("%5chis paragraph is indented", 't');
    printf("RET:%d\n", tmp);
    tmp = printf("%5chis paragraph is indented", 't');
    printf("RET:%d\n", tmp);

	printf("######## 008 c width #######\n");
    tmp = ft_printf("%5c now you see", '\0');
    printf("RET:%d\n", tmp);
    tmp = printf("%5c now you see", '\0');
    printf("RET:%d\n", tmp);

	printf("######## 009 c width #######\n");
    tmp = ft_printf("The number %7c represents luck", '7');
    printf("RET:%d\n", tmp);
    tmp = printf("The number %7c represents luck", '7');
    printf("RET:%d\n", tmp);

	return (0);
}
