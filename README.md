# fourtytwo-ft_printf

Repository for 42 Cursus ft_printf project.

## Reference

- https://www.tutorialspoint.com/c_standard_library/c_function_printf.htm

## 💡 About the project

> _The aim of this project is to make you recode the printf() function._

	This project is a custom implementation of the printf() function in the C programming language.
	You will mainly learn about using a variable number of arguments in this project.

	The printf() function is a widely used function in C that allows formatted output to the standard output stream.
	It provides a convenient way to display various types of data (strings, numbers, and other variables) in a specified format.

	The ft_printf() function is my own version of printf().
	It replicates most of the functionality of the standard printf() function while maintaing a customizable codebase.

For more detailed information, look at the [**subject of this project**](https://gitlab.com/0EFB6/fourtytwo-ft_printf/-/blob/main/ft_printf.pdf).

## 💪 Features
- Printing formatted output to the standard output stream
- Handling different conversion specifiers including `%c`, `%s`, `%p`, `%d`, `%i`, `%u`, `%x`, `%X`, `%%`
- Manage any combination of the following flags and the field minimum width under all conversions: `-`, `0`, `.`
- Manage all the following flags: `#`, ` `, `+`

## 🛠️ Usage

### Requirements

The function is written in C language and thus needs the **`gcc` or `cc` compiler** and some standard **C libraries** to run.

### Instructions - Using it in your code

To use the function in your code, simply compile it and include its library:

```shell
make all
gcc -Wall -Wextra -Werror -I include -L. -libftprintf main.c -o myexecutable
./myexecutable
```

## 📋 Testing

There are 2 tests:
- Mandatory part test (testm)
- Bonus part test (testb)

Do head into the respective directory to test.

You only have to edit the `test<specifier>.c` main function and headers inside it, make sure they are all correct.
Then simply run this command (change <specifier> with desired test flags or speciiers) :

```shell
gcc -Wall -Werror -Wextra test<specifier>.c ft_*.c && ./a.out
```

OR

Run the shell script command:

```shell
./run
```

OR

You can also use this third party tester to fully test the project:

* [Tripouille/printfTester](https://github.com/Tripouille/printfTester)

## 📈 Progress

- [X] v1 (Pure variable implementation, fail to do bonus part - Redo in v2)
- [X] v2 (Struct implementation, have minor issues for bonus part - Freezed, continue in v3)
- [ ] v3 (Workind On)

## 📝 Status

Fixing issues for bonus part, pending for evaluation.

For Bonus part, `x precision` has errors, 002, 019, 022 and 023 test case.

Run this command for next testing.
`head -n 1575 v3/wilson3.log | tail -n +1362`